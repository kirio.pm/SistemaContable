
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<script>
	    !window.jQuery && document.write("<script src='../js/jquery.min.js'><\/script>");
	</script>
	<title>Sistema Contable</title>
 
        <link rel="stylesheet" type="text/css" href="../css/style.css"/>
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>
</head>
 
<body>
<header id="main-header">		
		<a id="logo-header" href="#">
			<span class="site-name">Sistema Contable</span>
			<span class="site-desc"></span>
		</a>
 
		<nav>		
			<ul>
			<?php
			echo "
			<li><a href=\"inicio.php\">Inicio</a></li>
            <li><a href=\"catalogo.php\">Catálogo de cuentas</a></li>
            <li><a href=\"libro_diario.php\">Libro Diario</a></li>
            <li><a href=\"balance_comprobacion.php\">Balance de Comprobación</a></li>
            <li><a href=\"estado_resultados.php\">Estado de Resultados</a></li>
            <li><a href=\"estado_capital.php\">Estado de Capital</a></li>
            <li><a href=\"balance_general.php\">Balance General</a></li>
            <li><a href=\"costos.php\">Área Costos</a></li>"
			;
			?>
			<li><a href="#logout" data-toggle="modal"><!-- <span class="glyphicon glyphicon-log-out"></span> --> &nbsp;Cerrar sesión</a></li>
			</ul>
		</nav><!-- / nav -->
 
	</header><!-- / #main-header -->
 
	
	<section id="main-content">
	
		<article>
			<header>
				<h1>Balance de Comprobación</h1>
			</header>
				
			
			<div class="content">
			<?php
include('conexion.php');

$query="select  DISTINCT mes,ano from libro_diario order by ano";
$result=mysql_query($query) or die(mysql_error());
echo "<table border=1>
<tr>
<th>Mes</th>
<th>Año</th>

</tr>";
while($r=mysql_fetch_assoc($result)) {
	$mes=$r['mes'];
	$ano=$r['ano'];
	echo "<tr>
	<td><a href=\"detalle_balance.php?a=".$ano."&m=".$mes."\">".$mes."</a></td>
	<td><a href=\"detalle_balance.php?a=".$ano."&m=".$mes."\">".$ano."</a></td>
	";
}
echo "</table>";
?>
			
			</div>
			
		</article> <!-- /article -->
	
	</section> <!-- / #main-content -->
 
	
	
	<footer id="main-footer">
		<p>&copy; 2016 <a href="http://FranciscoAMK.com">Universidad de El Salvador</a></p>
	</footer> <!-- / #main-footer -->
 <?php include("modal.php"); ?>

 <script src="../js/bootstrap.min.js"></script>
	
</body>
</html>