<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Sistema Contable</title>
 
        <link rel="stylesheet" type="text/css" href="../css/style.css"/>
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>
     <script>
        !window.jQuery && document.write("<script src='../js/jquery.min.js'><\/script>");
    </script>
</head>
 
<body>
	
	<header id="main-header">
		
		<a id="logo-header" href="#">
			<span class="site-name">Sistema Contable</span>
			<span class="site-desc"></span>
		</a> <!-- / #logo-header -->
 
		<nav>
		
			<ul>
			<?php
			echo "
			<li><a href=\"inicio.php\"> Inicio</a></li>
			<li><a href=\"catalogo.php\"> Catalogo de cuentas</a></li>
			<li><a href=\"libro_diario.php\">Libro Diario </a></li>
			<li><a href=\"balance_comprobacion.php\">Blance de Comprobacion</a></li>
			<li><a href=\"estado_resultados.php\">Estado de Resultados</a></li>
			<li><a href=\"estado_capital.php\">Estado de capital</a></li>
			<li><a href=\"balance_general.php\"> Balance General</a></li>
			<li><a href=\"costos.php\"> Area Costos</a></li>"

			;
				?>
			<li><a href="#logout" data-toggle="modal"><span class="glyphicon glyphicon-log-out"></span> &nbsp;Cerrar sesión</a></li>
			</ul>
		</nav><!-- / nav -->
 
	</header><!-- / #main-header -->
 
	
	<section id="main-content">
	
		<article>
			<header>
				
			</header>
			
			
			
			<div class="content">
			
				<div class="row">
			<div class="col-lg-12 text-center">
				
				<h2>Empleados</h2>
			</div>
			<br><br>
			<table class='table table-bordered table-striped table-hover'>
				<thead>
					<tr>
						<th class="text-center">Empleado</th>
						<th class="text-center">Cargo</th>
						<th class="text-center">SM</th>
						<th class="text-center">ISSS*</th>
						<th class="text-center">ISSS</th>
						<th class="text-center">AFP*</th>
						<th class="text-center">AFP</th>
						<th class="text-center">SD</th>
						<th class="text-center">VC</th>
						<th class="text-center">AG</th>
						<th class="text-center">SMT</th>
						<th class="text-center">AMP</th>
						<th class="text-center">Salario</th>
					</tr>
				</thead>
				<tbody>
					
						<?php 
						if(!isset($link)) { include("conexion.php");}
						$sql = "SELECT * FROM empleados";
						$ejecutar_consulta = $link->query($sql);
						if($ejecutar_consulta->num_rows > 0){
							while ($reg = $ejecutar_consulta->fetch_assoc()) {
								echo "<tr align='right'>";
								echo "<td align='left'>".($reg["primer_nombre"])." ".($reg["primer_apellido"])."</td>";
								echo "<td align='left'>".($reg["cargo"])."</td>";
								echo "<td>".number_format($reg["salario_mensual_contratado"],2)."</td>";
								echo "<td>".number_format($reg["isss_trabajador"],2)."</td>";
								echo "<td>".number_format($reg["isss_patrono"],2)."</td>";
								echo "<td>".number_format($reg["afp_trabajador"],2)."</td>";
								echo "<td>".number_format($reg["afp_patrono"],2)."</td>";
								echo "<td>".number_format($reg["salario_diario"],2)."</td>";
								echo "<td>".number_format($reg["vacaciones"],2)."</td>";
								echo "<td>".number_format($reg["aguinaldo"],2)."</td>";
								echo "<td>".number_format($reg["salario_mensual"],2)."</td>";
								echo "<td>".number_format($reg["aportaciones_mensuales_patrono"],2)."</td>";
								echo "<td>".number_format($reg["pago_salario_patrono"],2)."</td>";
								echo "</tr>";
							}
						}
						?>
						<?php 
						$sql = "SELECT sum(pago_salario_patrono) total FROM empleados";
						$ejecutar_consulta = $link->query($sql);
						$total = $ejecutar_consulta->fetch_assoc();
						?>
						<tr>
							<td colspan="12" class="text-right"><strong>Total salarios:</strong></td>
							<td class="text-right"><?php echo number_format($total["total"],2); ?></td>
						</tr>
				</tbody>
			</table>
			<br><br><br><hr>
			<div class="col-lg-6">
				<h3>Abreviaturas</h3>
				<br>
				<div class="col-lg-6">
					<p><strong>SM: </strong>Salario Mensual</p>
					<p><strong>ISSS*: </strong>ISSS empleado</p>
					<p><strong>ISSS: </strong>ISSS Patrono</p>
					<p><strong>AFP*: </strong>AFP empleado</p>
					<p><strong>AFP: </strong>AFP Patrono</p>
				</div>
				<div class="col-lg-6 pull-left">
					<p><strong>SD: </strong>Salario Diario</p>
					<p><strong>VC: </strong>Vacaciones</p>
					<p><strong>AG: </strong>Aguinaldo</p>
					<p><strong>SMT: </strong>Salario mensual trabajador</p>
					<p><strong>AMP: </strong>Aportaciones Mensuales Patrono</p>
				</div>
			</div>
		</div>

			</div>
			
		</article> <!-- /article -->
	
	</section> <!-- / #main-content -->
 
	
	
	<footer id="main-footer">
		<p>&copy; 2016 <a href="http://FranciscoAMK.com">Universidad de El Salvador</a></p>
	</footer> <!-- / #main-footer -->
 <?php include("modal.php"); ?>

 <script src="../js/bootstrap.min.js"></script>
	
</body>
</html>