<?php 
	include("sesion.php");
	if(!$_COOKIE["sesion"]){
		header("Location: salir.php");
	}
?>	

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	
	<link rel="stylesheet" type="text/css" href="../css/style.css"/>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>
	
	<script>
	    !window.jQuery && document.write("<script src='../js/jquery.min.js'><\/script>");
	</script>
	<title>Sistema Contable</title>  
</head>
 
<body>	
	<header id="main-header">		
		<a id="logo-header" href="#">
			<span class="site-name">Sistema Contable</span>
			<span class="site-desc"></span>
		</a>

		<nav>		
			<ul>
			<?php
			echo "
			<li><a href=\"inicio.php\">Inicio</a></li>
            <li><a href=\"catalogo.php\">Catálogo de Cuentas</a></li>
            <li><a href=\"libro_diario.php\">Libro Diario</a></li>
            <li><a href=\"balance_comprobacion.php\">Balance de Comprobación</a></li>
            <li><a href=\"estado_resultados.php\">Estado de Resultados</a></li>
            <li><a href=\"estado_capital.php\">Estado de Capital</a></li>
            <li><a href=\"balance_general.php\">Balance General</a></li>
            <li><a href=\"costos.php\">Área Costos</a></li>"
			;
			?>
			<li><a href="#logout" data-toggle="modal"><!-- <span class="glyphicon glyphicon-log-out"></span> --> &nbsp;Cerrar sesión</a></li>
			</ul>
		</nav><!-- / nav -->
 
	</header><!-- / #main-header -->
 
	<section id="main-content">
	
		<article>
			<header></header>
			
			<h1>¡Bienvenido!</h1>
			
			<center><img src="../imagenes/pan.jpg" alt="Gatito" align="middle" height="400" width="600" /></center>
			
			<div class="content">
			<p style="color:black"><b>Modelo de empresa productora de alimentos funcionales con enfoque en productos de molinería y panadería.</b></p>
			<p style="color:black"><b>Visión</b></p>
			<p>“Ser una empresa innovadora en el desarrollo de productos con valor agregado para el tema de salud que contribuya al crecimiento económico del sector a través de la comercialización de nuestros productos en mercados internacionales que además contribuyan a la salud de los consumidores.”
			</p>
			<p style="color:black"><b>Misión</b></p>
			<p>“Producir Alimentos Funcionales con valor agregado a la salud del consumidor contribuyendo a la calidad de vida de los mismos, con lo cual posicionar los productos procedentes de El Salvador en mercados internacionales, generando un crecimiento del sector en el país.”</p>
 
			</div>
			
		</article> <!-- /article -->
	
	</section> <!-- / #main-content -->
 
	<!-- / #main-footer -->
	<?php include("modal.php"); ?>

 <script src="../js/bootstrap.min.js"></script>
	
</body>
</html>