<?php 
	include("sesion.php");
	if(!$_COOKIE["sesion"]){
		header("Location: salir.php");
	}
?>	

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<meta charset="UTF-8"/>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	
	<link rel="stylesheet" type="text/css" href="../css/style.css"/>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>
    <link rel="stylesheet" href="../jquery-ui/jquery-ui.css" >
    
	<script src="../jquery-ui/external/jquery/jquery.js"></script>
	<script src="../jquery-ui/jquery-ui.js"></script>
	<script src="../js/Scripts1.js"></script>
	
	<script>
	    !window.jQuery && document.write("<script src='../js/jquery.min.js'><\/script>");
	</script>
	<title>Estado de Costos de Inventarios</title>  
</head>

<!-- <html>
<head>
	<title></title>

	<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
	<meta charset="utf-8">
	<link href="jquery-ui/jquery-ui.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/estilos.css"/>
	<script src="jquery-ui/external/jquery/jquery.js"></script>
	<script src="jquery-ui/jquery-ui.js"></script>
	<script src="js/Scripts1.js"></script>

</head> -->


<body>
	<header id="main-header">		
		<a id="logo-header" href="#">
			<span class="site-name">Sistema Contable</span>
			<span class="site-desc"></span>
		</a>

		<nav>		
			<ul>
			<?php
			echo "
			<li><a href=\"inicio.php\">Inicio</a></li>
            <li><a href=\"catalogo.php\">Catálogo de cuentas</a></li>
            <li><a href=\"libro_diario.php\">Libro Diario</a></li>
            <li><a href=\"balance_comprobacion.php\">Balance de Comprobación</a></li>
            <li><a href=\"estado_resultados.php\">Estado de Resultados</a></li>
            <li><a href=\"estado_capital.php\">Estado de Capital</a></li>
            <li><a href=\"balance_general.php\">Balance General</a></li>
            <li><a href=\"costos.php\">Área Costos</a></li>"
			;
			?>
			<li><a href="#logout" data-toggle="modal"><!-- <span class="glyphicon glyphicon-log-out"></span> --> &nbsp;Cerrar sesión</a></li>
			</ul>
		</nav><!-- / nav -->
 
	</header><!-- / #main-header -->
	<br>
	<center> <article>
 <div class="content">
<div  id="EstadoDeCostos" style=" width: 45%; border: 1px solid black; padding: 10px;">
	<h1>Estado de Costos de Inventarios</h1>
	<form style="border: 1px solid black; padding: 15px; border-radius: 5px;">
	<label>Ingreso por Ventas</label><input  type="number" disabled="true" id="inVenta">
	<br><br><label >-Costo de lo vendido</label><input id="CVendido"  name="Costo de lo vendido" disabled="true"  value="">
	<br><br><label>=Utilidades Brutas o <br>de comercializacion</label><input id="UBrutas" type="number" name="utilidadesBrutas" disabled="true">
	<br><br><label>-Gastos de Operación</label><input id="Goperacion" name="Goperacion" disabled="true">
	<br><br><label>-Gastos de Venta</label><input  id="GVenta" disabled="true">
	<br><br><label>=Utilidades de Operación</label><input  id="UtOperacion" disabled="true">
	<br><br><label>-Gastos Financieros</label><input  disabled="true"  id="GFinancieros">
	<br><br><label>=Utilidades antes de Otros Ingresos</label><input disabled="true"  id="UAOIngresos">
	<br><br><label>+Otros Ingresos</label><input disabled="true"  id="otrosIngreso">
	<br><br><label>=Utilidades antes de impuestos</label><input disabled="true"  id="UtAntesImpuestos">
	<br><br><label>-Impuesto Sobre la Renta</label><input disabled="true"  id="Renta">
	<br><br><label>=Utilidades</label><input  id="Utilidades" disabled="true">
	
	</form>
	
</div>
</div>
</article>
</center>
<?php include("modal.php"); ?>

 <script src="../js/bootstrap.min.js"></script>

</body>
</html>