<?php

function validar_codigo($codigo){
	require 'conexion2.php';
	$codigo_existe = false;
	$consulta = "SELECT  partida FROM libro_diario  WHERE  partida = ?";
	$resultado = mysqli_prepare($conexion,$consulta);
	$isCorrecto = mysqli_stmt_bind_param($resultado,"i",$codigo);
	$isCorrecto = mysqli_stmt_execute($resultado);
	
	if($isCorrecto){
		$isCorrecto = mysqli_stmt_bind_result($resultado,$codigo_obtenido);
		if(mysqli_stmt_fetch($resultado)){
			$codigo_existe = true;
			}
		}else{
			echo "Error en la ejecucion de la consulta";
			}
		
		return $codigo_existe;
	}
	
	function ingresar_libroDiario($codigo,$dia,$mes,$anio,$descripcion){
		require 'conexion2.php';
		$result = false;
		$sql= "INSERT INTO libro_diario(dia,mes,ano,partida,descripcion) VALUES( ?, ?, ?, ?,?)";
		$resultado = mysqli_prepare($conexion,$sql);
		$isCorrecto = mysqli_stmt_bind_param($resultado,"isiis",$dia,$mes,$anio,$codigo,$descripcion);
		$isCorrecto = mysqli_stmt_execute($resultado);
		if($isCorrecto){
			$result = true;
		}
		else{
			echo "No se pudo realizar la  insercion de datos correctamente<br>";
			}
			return $result;
		}
		
		function ingresar_detalleLibro($cuenta,$debe,$haber,$codigo){
		require 'conexion2.php'; 
		$result = false;
		$sql= "INSERT INTO detalle_libro_diario(id_movimiento,cuenta,deber,haber) VALUES( ? , ? , ?, ?)";
		$resultado = mysqli_prepare($conexion,$sql);
		
		for($i =0; $i < count($cuenta); $i++){
			$isCorrect =mysqli_stmt_bind_param($resultado,"iidd",$codigo,$cuenta[$i],$debe[$i],$haber[$i]);
			$isCorrect =mysqli_stmt_execute($resultado);
			if($isCorrect){
				$result = true;
				}else{
					echo "Error al ingresar la cuenta No: ". $cuenta[$i] . "<br>";
					}
			
			}
			return $result;
		}
			function get_idMovimiento($codigo){
		require 'conexion2.php';
		$result = "";
		$sql= "SELECT id_movimiento FROM libro_diario WHERE partida = ?";
		$resultado = mysqli_prepare($conexion,$sql);
		$isCorrect =mysqli_stmt_bind_param($resultado,"i",$codigo);
		$isCorrect = mysqli_stmt_execute($resultado);
		if($isCorrect){
						$isCorrect = mysqli_stmt_bind_result($resultado,$id);
						mysqli_stmt_fetch($resultado);
						$result = $id;
						}	else{
							echo "No se puedo realizar la consulta<br>";
							}
							return $result;
				}
	

?>