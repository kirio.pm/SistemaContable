
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Sistema Contable</title>
 
       <link rel="stylesheet" type="text/css" href="../css/style.css"/>
       <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>
        <script>
	    !window.jQuery && document.write("<script src='../js/jquery.min.js'><\/script>");
	</script>

</head>
 
<body>
	
	<header id="main-header">
		
		<a id="logo-header" href="#">
			<span class="site-name">Sistema Contable</span>
			<span class="site-desc"></span>
		</a> <!-- / #logo-header -->
 
		<nav>		
			<ul>
			<?php
			echo "
			<li><a href=\"inicio.php\">Inicio</a></li>
            <li><a href=\"catalogo.php\">Catálogo de cuentas</a></li>
            <li><a href=\"libro_diario.php\">Libro Diario</a></li>
            <li><a href=\"balance_comprobacion.php\">Balance de Comprobación</a></li>
            <li><a href=\"estado_resultados.php\">Estado de Resultados</a></li>
            <li><a href=\"estado_capital.php\">Estado Capital</a></li>
            <li><a href=\"balance_general.php\">Balance General</a></li>
            <li><a href=\"costos.php\">Área Costos</a></li>"
			;
			?>
			<li><a href="#logout" data-toggle="modal"><!-- <span class="glyphicon glyphicon-log-out"></span> --> &nbsp;Cerrar sesión</a></li>
			</ul>
		</nav><!-- / nav -->
 
	</header><!-- / #main-header -->
 
	
	<section id="main-content">
	
		<article>
			<header>
				
			</header>
			
			<h1>Libro diario</h1>
			
			<div class="content">
             <a class="btn btn-success btn-lg btn-block btn-raised" href="registrar_diarioCorriente.php">Agregar nueva partida</a><br>
            <a class="btn btn-success btn-lg btn-block btn-raised" href="registrar_diarioCompraVenta.php">Agregar partida de compra o venta</a> 
            <br>			
            <?php 
include('conexion.php');

$query="select id_movimiento,dia,mes,ano,partida,descripcion from libro_diario order by mes and ano";
$result=mysql_query($query) or die(mysql_error());
echo "<table border=2>
<tr>
<th>Mes</th>
<th>Dia</th>

<th>Año</th>
<th>Partida</th>
<th>Descripcion</th>
</tr>";


while($r=mysql_fetch_assoc($result)) {
	echo"<tr>
	<td><a href=\"detalle_diario.php?m=".$r['id_movimiento']."\">".$r['mes']."</td></a>
	<td><a href=\"detalle_diario.php?m=".$r['id_movimiento']."\">".$r['dia']."</td></a>
	<td><a href=\"detalle_diario.php?m=".$r['id_movimiento']."\">".$r['ano']."</td></a>
	<td><a href=\"detalle_diario.php?m=".$r['id_movimiento']."\">".$r['partida']."</td></a>
	<td><a href=\"detalle_diario.php?m=".$r['id_movimiento']."\">".$r['descripcion']."</td></a>
	</tr>";
	}
echo "</table>";
?>
			
			</div>
			
		</article> <!-- /article -->
	
	</section> <!-- / #main-content -->
 
	
	
	<footer id="main-footer">
		<p>&copy; 2016 <a href="http://FranciscoAMK.com">Universidad de El Salvador</a></p>
	</footer> <!-- / #main-footer -->
 <?php include("modal.php"); ?>

 <script src="../js/bootstrap.min.js"></script>
	
</body>
</html>