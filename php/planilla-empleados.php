<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Sistema Contable</title>
 
        <link rel="stylesheet" type="text/css" href="../css/style.css"/>
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>
</head>
 
<body>
	
	<header id="main-header">
		
		<a id="logo-header" href="#">
			<span class="site-name">Sistema Contable</span>
			<span class="site-desc"></span>
		</a> <!-- / #logo-header -->
 
		<nav>
		
			<ul>
			<?php
			echo "
			<li><a href=\"inicio.php\"> Inicio</a></li>
			<li><a href=\"catalogo.php\"> Catalogo de cuentas</a></li>
			<li><a href=\"libro_diario.php\">Libro Diario </a></li>
			<li><a href=\"balance_comprobacion.php\">Blance de Comprobacion</a></li>
			<li><a href=\"estado_resultados.php\">Estado de Resultados</a></li>
			<li><a href=\"estado_capital.php\">Estado de capital</a></li>
			<li><a href=\"balance_general.php\"> Balance General</a></li>
			<li><a href=\"costos.php\"> Area Costos</a></li>"

			;
				?>
			<li><a href="#logout" data-toggle="modal"><span class="glyphicon glyphicon-log-out"></span> &nbsp;Cerrar sesión</a></li>
			</ul>
			
		</nav><!-- / nav -->
 
	</header><!-- / #main-header -->
 
	
	<section id="main-content">
	
		<article>
			<header>
				<h1>Estado de Resultados</h1>
			</header>
			
			
			
			<div class="content">
	<div class="row row-offcanvas row-offcanvas-right">
			<div class="col-xs-12 col-sm-9">
				<div class="page-header">
        			<h3>Planilla de empleados</h3>
        		</div>
        		<div class="row">
        			<div class="col-lg-12">
        				<h4><a href="ver-planilla.php" target="_blank">Haga click aquí para ver la planilla</a></h4>
        			</div>
        		</div>
        	</div><!--/span-->
        	
        </div>			

			</div>
			
		</article> <!-- /article -->
	
	</section> <!-- / #main-content -->
 
	
	
	<footer id="main-footer">
		<p>&copy; 2016 <a href="http://FranciscoAMK.com">Universidad de El Salvador</a></p>
	</footer> <!-- / #main-footer -->
 
	
</body>
</html>