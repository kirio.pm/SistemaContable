﻿
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Sistema Contable</title>
 
        <link rel="stylesheet" href="../css/style.css">
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>
	
</head>
 
<body>
	
	<header id="main-header">
		
		<a id="logo-header" href="#">
			<span class="site-name">Sistema Contable</span>
			<span class="site-desc"></span>
		</a> <!-- / #logo-header -->
 
		<nav>
		
			<ul>
			<?php
			echo "
			<li><a href=\"inicio.php\"> Inicio</a></li>
			<li><a href=\"catalogo.php\"> Catalogo de cuentas</a></li>
			<li><a href=\"libro_diario.php\">Libro Diario </a></li>
			<li><a href=\"balance_comprobacion.php\">Blance de Comprobacion</a></li>
			<li><a href=\"estado_resultados.php\">Estado de Resultados</a></li>
			<li><a href=\"estado_capital.php\">Estado de capital</a></li>
			<li><a href=\"balance_general.php\"> Balance General</a></li>
			<li><a href=\"costos.php\"> Area Costos</a></li>"

			;
				?>
			<li><a href="#logout" data-toggle="modal"><span class="glyphicon glyphicon-log-out"></span> &nbsp;Cerrar sesión</a></li>
			</ul>
			
		</nav><!-- / nav -->
 
	</header><!-- / #main-header -->
 
	
	<section id="main-content">
	
		<article>
			<header>
				<h1>Registro Libro diario</h1>
			</header>
			
			
		</article> <!-- /article -->
        <div class="content" id="principal">
          <p>
            <?php

				function ingresarDatos(){
					include 'registra_diariodb.php';
					$codigo_diario=$_POST['codigo_libro_diario'];
					$dia = $_POST['dia'];
					$mes = $_POST['mes'];
					$anio = $_POST['anio'];
					$cuentas_afectadas = $_POST['cuenta'];
					$debe = $_POST['deber'];
					$haber = $_POST['haber'];
					$descripcion = $_POST['descripcion'];
					
				if(validar_codigo($codigo_diario)){
				echo "El codigo ya existe<br>";
				
				
					}else{
					$is_correctoIngresoLibro= ingresar_libroDiario($codigo_diario,$dia,$mes,$anio,$descripcion);
					$id = get_idMovimiento($codigo_diario);
					$is_correctoIngresoDetalle= ingresar_detalleLibro($cuentas_afectadas,$debe,$haber,$id);
					if($is_correctoIngresoDetalle & $is_correctoIngresoLibro){
						echo "<h3>Transaccion exitosa</h3><br>";
						
						}else{
							echo "<strong>La transacción Fallo !!! </strong><br>";
							}
					}
				}
				ingresarDatos();
		?>
        <br>
        <a href="libro_diario.php" class="btn btn-primary">Volver a libro diario</a>
        <a href="registrar_diarioCorriente.php" class="btn btn-info">Ingresar una partida corriente</a>
        <a href="registrar_diarioCompraVenta.php" class="btn btn-info"">Ingresar una partida de compra o venta</a>
    
        </div>
	</section> <!-- / #main-content -->
 
	<footer id="main-footer">
		<p>&copy; 2016 <a href="http://FranciscoAMK.com">Universidad de El Salvador</a></p>
        
	</footer> <!-- / #main-footer -->

</body>
</html>
