<?php

require_once("../dompdf/dompdf_config.inc.php");
include('conexion.php');
include('sesion.php');

$codigoHTML='
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../css/stylePdf.css"/>
<title>Documento sin título</title>
</head>
<body>
<h1>Estado De Resultados</h1>
<h2>Panadería Freshbread</h2>
<h2>Del 1 de '.$_SESSION['mes1'].' al 30 del Año '.$_SESSION['anio1'].' </h2>

<table border=1>
  <tr>
<th>Cuenta</th>
<th>Deber</th>
<th>Haber</th>
</tr>';

		$debe=0;
		$habe=0;
		$sDebe=0;
		$sHaber=0;
		$UNtotal=0;
//Ingresos
		$query2="select nombre_cuenta,sum(deber) as deber,sum(haber) as haber from detalle_libro_diario inner join cuenta on(codigo_mayor=cuenta) inner join libro_diario on(detalle_libro_diario.id_movimiento=libro_diario.id_movimiento) where mes=". "'". $_SESSION['mes1']. "'" ." and ano=".$_SESSION['anio1']." and er=0 group by cuenta order by tipo_cuenta ";
		$result2=mysql_query($query2) or die(mysql_error());
		$r2=mysql_fetch_assoc($result2);
$codigoHTML.='		
		<tr>
		<th colspan="3">Ingresos</th>
		</tr>
		<tr>
		<td>'.$r2['nombre_cuenta'].'</td>
		<td>$'.$r2['deber'].'</td>
		<td>$'.$r2['haber'].'</td>

		</tr>
<tr> <th colspan="3">Egresos</th></tr>	';		
$sHaber2=$r2['haber'];
//gastos
		$query="select nombre_cuenta,sum(deber) as deber,sum(haber) as haber from detalle_libro_diario inner join cuenta on(codigo_mayor=cuenta) inner join libro_diario on(detalle_libro_diario.id_movimiento=libro_diario.id_movimiento) where mes=". "'". $_SESSION['mes1']. "'" ." and ano=".$_SESSION['anio1']." and er=1 group by cuenta order by tipo_cuenta ";
		$result=mysql_query($query) or die(mysql_error());

		while($r=mysql_fetch_assoc($result)){
$codigoHTML.='

<tr>
<td>'.$r['nombre_cuenta'].'</td>
<td>$'.$r['deber'].'</td>
<td>$'.$r['haber'].'</td>
</tr>';

$sDebe=$sDebe+$r['deber'];
$sHaber=$sHaber+$r['haber'];
}
$UNtotal=$sHaber2-$sDebe-$sHaber;



$codigoHTML.='
	<tr>
		<th></th>
		<th colspan="2">Total</th>
	</tr>
	<tr>
		<td>Utilidades Netas(+)/Perdidas(-)</td>
		<td colspan="2">$'.$UNtotal.'</td>
	</tr>
</table>
</body>
</html>';
//$codigoHTML=utf8_decode($codigoHTML);
$dompdf=new DOMPDF();
$dompdf->load_html($codigoHTML);
ini_set("memory_limit","128M");
$dompdf->render();
$dompdf->stream("Reporte_Estado_Resultado.pdf");
?>