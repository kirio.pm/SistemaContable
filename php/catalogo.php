	
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Sistema Contable</title>
	<script>
	    !window.jQuery && document.write("<script src='../js/jquery.min.js'><\/script>");
	</script>
 
 
<link rel="stylesheet" type="text/css" href="../css/style.css"/>
<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css"/>
<link rel="stylesheet" type="text/css" href="../css/estilos.css"/>
</head>
 
<body>
	
	<header id="main-header">		
		<a id="logo-header" href="#">
			<span class="site-name">Sistema Contable</span>
			<span class="site-desc"></span>
		</a>

		<nav>		
			<ul>
			<?php
			echo "
			<li><a href=\"inicio.php\">Inicio</a></li>
            <li><a href=\"catalogo.php\">Catálogo de cuentas</a></li>
            <li><a href=\"libro_diario.php\">Libro Diario</a></li>
            <li><a href=\"balance_comprobacion.php\">Balance de Comprobación</a></li>
            <li><a href=\"estado_resultados.php\">Estado de Resultados</a></li>
            <li><a href=\"estado_capital.php\">Estado de Capital</a></li>
            <li><a href=\"balance_general.php\">Balance General</a></li>
            <li><a href=\"costos.php\">Área Costos</a></li>"
			;
			?>
			<li><a href="#logout" data-toggle="modal"><!-- <span class="glyphicon glyphicon-log-out"></span> --> &nbsp;Cerrar sesión</a></li>
			</ul>
		</nav><!-- / nav -->
 
	</header><!-- / #main-header -->
 
	
	<section id="main-content">
	
		<article>
			<header>
				
			</header>
			<h1>Catálago de cuentas</h1>
			<?php
include('conexion.php');

$query="select * from cuenta order by tipo_cuenta";
$resultado=mysql_query($query) or die(mysql_error());

echo "<br><b><a class='btn btn-success btn-lg btn-block btn-raised' href=\"registro_cuenta.php\"> AGREGAR NUEVA CUENTA </a></b>";

echo "<br><table border=1>
<tr>
<th>Tipo de Cuenta</th>
<th> Codigo de Mayor </th>
<th> Nombre de Cuenta </th>
<th> Descripcion </th></tr>";
while($r=mysql_fetch_assoc($resultado)){
	$q2="select nombre_tipo_cuenta from tipo_cuenta where id_tipo_cuenta=".$r['tipo_cuenta'];
	echo "<tr>
	<td>";
	echo mysql_result(mysql_query($q2),0);
	echo "<td>".$r['codigo_mayor']."
	<td>".$r['nombre_cuenta']."
	<td>".$r['descripcion']."
	</tr>";
	};
echo "</table>";
?>
			</div>
			
		</article> <!-- /article -->
	
	</section> <!-- / #main-content -->
 
	
	
	<footer id="main-footer">
		<p>&copy; 2016 <a href="http://FranciscoAMK.com">Universidad de El Salvador</a></p>
	</footer> <!-- / #main-footer -->
	<?php include("modal.php"); ?>

 <script src="../js/bootstrap.min.js"></script>
 
	
</body>
</html>