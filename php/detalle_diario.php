<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Sistema Contable</title>
 
       <link rel="stylesheet" type="text/css" href="../css/style.css"/>
       <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css"/>
       <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>
       <script>
	    !window.jQuery && document.write("<script src='../js/jquery.min.js'><\/script>");
	</script>
</head>
 
<body>
	
	<header id="main-header">		
		<a id="logo-header" href="#">
			<span class="site-name">Sistema Contable</span>
			<span class="site-desc"></span>
		</a>

		<nav>		
			<ul>
			<?php
			echo "
			<li><a href=\"inicio.php\">Inicio</a></li>
            <li><a href=\"catalogo.php\">Catálogo de cuentas</a></li>
            <li><a href=\"libro_diario.php\">Libro Diario</a></li>
            <li><a href=\"balance_comprobacion.php\">Balance de Comprobación</a></li>
            <li><a href=\"estado_resultados.php\">Estado de Resultados</a></li>
            <li><a href=\"estado_capital.php\">Estado de Capital</a></li>
            <li><a href=\"balance_general.php\">Balance General</a></li>
            <li><a href=\"costos.php\">Área Costos</a></li>"
			;
			?>
			<li><a href="#logout" data-toggle="modal"><!-- <span class="glyphicon glyphicon-log-out"></span> --> &nbsp;Cerrar sesión</a></li>
			</ul>
		</nav><!-- / nav -->
 
	</header><!-- / #main-header -->
 
	
	<section id="main-content">
	
		<article>
			<header>
				
			</header>
			<br>
			<h1>Detalle</h1>
			
			
			
			<div class="content">
				<?php
include('conexion.php');

if(isset($_GET['m'])){
	$movimiento=$_GET['m'];
	
	$query="select cuenta,deber,haber from detalle_libro_diario where id_movimiento=".$movimiento;
	$result=mysql_query($query) or die(mysql_error());
	echo "<table border=1>
	<tr>
	<th>Cuenta</th>
	<th>Deber</th>
	<th>Haber</th>
	</tr>";
	while($r=mysql_fetch_assoc($result)){
		echo "<tr><td>".mysql_result(mysql_query("select nombre_cuenta from cuenta where codigo_mayor=".$r['cuenta']),0)."</td>
		<td>$".$r['deber']."</td>
		<td>$".$r['haber']."</td>
		</tr>";
		}
	echo"</table>";
	}
	else{
	header('location: libro_diario.php');		
		}

?>
			</div>
			
		</article> <!-- /article -->
	
	</section> <!-- / #main-content -->
 
	
	
	<footer id="main-footer">
		<p>&copy; 2016 <a href="http://FranciscoAMK.com">Universidad de El Salvador</a></p>
	</footer> <!-- / #main-footer -->
	<?php include("modal.php"); ?>

 <script src="../js/bootstrap.min.js"></script>
 
	
</body>
</html>