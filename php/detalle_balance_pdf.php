<?php

require_once("../dompdf/dompdf_config.inc.php");
include('conexion.php');
include('sesion.php');
		$UtilidadP=array('utilidad(+)/perdida(-)');
		$array=$UtilidadP;
		$Participaciones=array('Nuevo Capital Social');
		$array2=$Participaciones;
		
		$sDebe=0;
		$sHaber=0;
		$sDebe2=0;
		$sHaber2=0;
		$sDebe3=0;
		$sHaber3=0;
		$sDebe4=0;
		$sHaber4=0;

$codigoHTML='
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../css/stylePdf.css"/>
<title>Documento sin título</title>
</head>
<body>
<h1>Balance de comprobacion </h1>
<h2>Panadería Freshbread</h2>
<h2>Del 1 de '.$_SESSION['mes'].' al 30 del año '.$_SESSION['anio'].' </h2>


<table border=1>
  		<tr>
		<th>Cuenta</th>
		<th>Deber</th>
		<th>Haber</th>
		</tr>

		<tr>
		<th colspan="3">Activos</th>
		</tr>';
		

		$query="select nombre_cuenta,sum(deber-haber) as deber,(haber=0) as haber from detalle_libro_diario inner join cuenta on(codigo_mayor=cuenta) inner join libro_diario on(detalle_libro_diario.id_movimiento=libro_diario.id_movimiento) where mes=". "'". $_SESSION['mes']. "'" ." and ano=".$_SESSION['anio']." and er=4 group by cuenta order by tipo_cuenta ";
		$result=mysql_query($query) or die(mysql_error());
		while($r=mysql_fetch_assoc($result)){

$codigoHTML.='

		<tr>
		<td>'.$r['nombre_cuenta'].'</td>
		<td>$'.$r['deber'].'</td>
		<td>$0</td>
		</tr>';
		$sDebe=$sDebe+$r['deber'];
			
		}


		//participaciones
$codigoHTML.='		
		<tr>
		<th colspan="3">Pasivos</th>
		</tr>';

		$query2="select nombre_cuenta,(deber=0) as deber,sum(haber-deber) as haber from detalle_libro_diario inner join cuenta on(codigo_mayor=cuenta) inner join libro_diario on(detalle_libro_diario.id_movimiento=libro_diario.id_movimiento) where mes=". "'". $_SESSION['mes']. "'" ." and ano=".$_SESSION['anio']." and er=5 group by cuenta order by tipo_cuenta ";
		$result2=mysql_query($query2) or die(mysql_error());
		while($r2=mysql_fetch_assoc($result2)){

$codigoHTML.='

		<tr>
		<td>'.$r2['nombre_cuenta'].'</td>
		<td>$0</td>
		<td>$'.$r2['haber'].'</td>
		</tr>';
		$sHaber=$sHaber+$r2['haber'];
		}


		//Resultados
$codigoHTML.='		
		<tr>
		<th colspan="3">Resultados</th>
		</tr>';

		$query3="select nombre_cuenta,sum(deber) as deber,sum(deber) as haber from detalle_libro_diario inner join cuenta on(codigo_mayor=cuenta) inner join libro_diario on(detalle_libro_diario.id_movimiento=libro_diario.id_movimiento) where mes=". "'". $_SESSION['mes']. "'" ." and ano=".$_SESSION['anio']." and er=1 group by cuenta order by tipo_cuenta ";
		$result3=mysql_query($query3) or die(mysql_error());
		while($r3=mysql_fetch_assoc($result3)){

$codigoHTML.='

		<tr>
		<td>'.$r3['nombre_cuenta'].'</td>
		<td>$'.$r3['deber'].'</td>
		<td>$0</td>
		
		</tr>';
		$sDebe2=$sDebe2+$r3['deber'];
		}
	
$codigoHTML.='';

		$query4="select nombre_cuenta,(deber=0) as deber,sum(haber) as haber from detalle_libro_diario inner join cuenta on(codigo_mayor=cuenta) inner join libro_diario on(detalle_libro_diario.id_movimiento=libro_diario.id_movimiento) where mes=". "'". $_SESSION['mes']. "'" ." and ano=".$_SESSION['anio']." and er=0 group by cuenta order by tipo_cuenta ";
		$result4=mysql_query($query4) or die(mysql_error());
		while($r4=mysql_fetch_assoc($result4)){

$codigoHTML.='
		<tr>
		<td>'.$r4['nombre_cuenta'].'</td>
		<td>$0</td>
		<td>$'.$r4['haber'].'</td>
		</tr>';
		$sHaber2=$sHaber2+$r4['haber'];
		}

		//capital
$codigoHTML.='		
		<tr>
		<th colspan="3">Capital</th>
		</tr>';
	
		$query5="select nombre_cuenta,(deber=0) as deber,sum(haber-deber) as haber from detalle_libro_diario inner join cuenta on(codigo_mayor=cuenta) inner join libro_diario on(detalle_libro_diario.id_movimiento=libro_diario.id_movimiento) where mes=". "'". $_SESSION['mes']. "'" ." and ano=".$_SESSION['anio']." and er=3 group by cuenta order by tipo_cuenta ";
		$result5=mysql_query($query5) or die(mysql_error());
		while($r5=mysql_fetch_assoc($result5)){

$codigoHTML.='

		<tr>
		<td>'.$r5['nombre_cuenta'].'</td>
		<td>$0</td>
		<td>$'.$r5['haber'].'</td>
		</tr>';
		$sHaber3=$sHaber3+$r5['haber'];
		}

		
		
		$totalDebe=$sDebe+$sDebe2;
		$totalHaber=$sHaber+$sHaber2+$sHaber3;

$codigoHTML.='
		
	<tr>
		<th></th>
		<th colspan="2">Total</th>
	</tr>
	<tr>
		<td>totales</td>
		<td >$'.$totalDebe.'</td>
		<td >$'.$totalHaber.'</td>
	</tr>
</table>
</body>
</html>';
		//$codigoHTML=utf8_decode(utf8_encode($codigoHTML));
		$dompdf=new DOMPDF();
		$dompdf->load_html($codigoHTML);
		ini_set("memory_limit","256M");
		$dompdf->render();
		$dompdf->stream("Reporte_Balance_comprobacion.pdf");
?>