
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Sistema Contable</title>
	<script>
	    !window.jQuery && document.write("<script src='../js/jquery.min.js'><\/script>");
	</script>
       <link rel="stylesheet" type="text/css" href="../css/style.css"/>
       <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>
</head>
 
<body>
	
	<header id="main-header">		
		<a id="logo-header" href="#">
			<span class="site-name">Sistema Contable</span>
			<span class="site-desc"></span>
		</a>

		<nav>		
			<ul>
			<?php
			echo "
			<li><a href=\"inicio.php\">Inicio</a></li>
            <li><a href=\"catalogo.php\">Catálogo de cuentas</a></li>
            <li><a href=\"libro_diario.php\">Libro Diario</a></li>
            <li><a href=\"balance_comprobacion.php\">Balance de Comprobación</a></li>
            <li><a href=\"estado_resultados.php\">Estado de Resultados</a></li>
            <li><a href=\"estado_capital.php\">Estado de Capital</a></li>
            <li><a href=\"balance_general.php\">Balance General</a></li>
            <li><a href=\"costos.php\">Área Costos</a></li>"
			;
			?>
			<li><a href="#logout" data-toggle="modal"><!-- <span class="glyphicon glyphicon-log-out"></span> --> &nbsp;Cerrar sesión</a></li>
			</ul>
		</nav><!-- / nav -->
 
	</header><!-- / #main-header -->
 
	
	<section id="main-content">
	
		<article>
			<header>
				
			</header>
			
		<h1>Detalle de Mes</h1>
			
			<div class="content">
				<?php
include('conexion.php');
include('sesion.php');

if((!isset($_GET['m']))&&(!isset($_GET['a']))){
	header('location:balance_comprobacion.php');	
	}
	else{
		$mes=$_GET['m'];
		$ano=$_GET['a'];
		
		$_SESSION['mes']=$mes;
		$_SESSION['anio']=$ano;
		$sDebe=0;
		$sHaber=0;
		$sDebe2=0;
		$sHaber2=0;
		$sDebe3=0;
		$sHaber3=0;
		$sDebe4=0;
		$sHaber4=0;
		$sHaber5=0;
		$sHaber6=0;
		
		
	echo "<h4>Balance de comprobacion del mes de ".$mes." del año ".$ano."</h4>";
		
	echo "<table border=1>
		<tr>
		<th>Cuenta</th>
		<th>Deber</th>
		<th>Haber</th>
		</tr>
		<tr>
		<th>Activos</th>
		</tr>";
		$query="select nombre_cuenta,sum(deber-haber) as deber,(haber=0) as haber from detalle_libro_diario inner join cuenta on(codigo_mayor=cuenta) inner join libro_diario on(detalle_libro_diario.id_movimiento=libro_diario.id_movimiento) where mes=".json_encode($mes)." and ano=".$ano." and er=4 group by cuenta order by tipo_cuenta ";
		$result=mysql_query($query) or die(mysql_error());
		while($r=mysql_fetch_assoc($result)){
		echo "<tr>
		<td>".$r['nombre_cuenta']."</td>
		<td>$".$r['deber']."</td>
		<td>$".$sHaber6."</td>
		</tr>";
		$sDebe=$sDebe+$r['deber'];
		}
	echo "	<tr>
			<th>Pasivos</th>
			</tr>";
		$query2="select nombre_cuenta,(deber=0) as deber,sum(haber-deber) as haber from detalle_libro_diario inner join cuenta on(codigo_mayor=cuenta) inner join libro_diario on(detalle_libro_diario.id_movimiento=libro_diario.id_movimiento) where mes=".json_encode($mes)." and ano=".$ano." and er=5 group by cuenta order by tipo_cuenta ";
		$result2=mysql_query($query2) or die(mysql_error());
		while($r2=mysql_fetch_assoc($result2)){
			echo "
			<tr>
		
			<td>".$r2['nombre_cuenta']."</td>
			<td>$".$sHaber6."</td>
			<td>$".$r2['haber']."</td>
			</tr>";
			
			
			$sHaber2=$sHaber2+$r2['haber'];
		}

echo " 	<tr>
			<th>Resultados</th>
		</tr>";

		$query3="select nombre_cuenta,sum(deber) as deber,sum(haber) as haber from detalle_libro_diario inner join cuenta on(codigo_mayor=cuenta) inner join libro_diario on(detalle_libro_diario.id_movimiento=libro_diario.id_movimiento) where mes=".json_encode($mes)." and ano=".$ano." and er=1  group by cuenta order by tipo_cuenta ";
		$result3=mysql_query($query3) or die(mysql_error());
		while($r3=mysql_fetch_assoc($result3))
		{

echo "   

		<tr>
			<td>".$r3['nombre_cuenta']."</td>
			<td>$".$r3['deber']."</td>
			<td>$".$sHaber6."</td>
		</tr>";

			$sDebe2=$sDebe2+$r3['deber'];
		}

		$query4="select nombre_cuenta,sum(deber) as deber,sum(haber) as haber from detalle_libro_diario inner join cuenta on(codigo_mayor=cuenta) inner join libro_diario on(detalle_libro_diario.id_movimiento=libro_diario.id_movimiento) where mes=".json_encode($mes)." and ano=".$ano." and er=0  group by cuenta order by tipo_cuenta ";
		$result4=mysql_query($query4) or die(mysql_error());
		while($r4=mysql_fetch_assoc($result4)){
		echo"
		<tr>
		
			<td>".$r4['nombre_cuenta']."</td>
			<td>$".$sHaber6."</td>
			<td>$".$r4['haber']."</td>

		</tr>";
			$sHaber3=$sHaber3+$r4['haber'];
		}


echo "		<tr>
			<th>Capital</th>
			</tr>
			";
		$query5="select nombre_cuenta,(deber=0) as deber,sum(haber-deber) as haber from detalle_libro_diario inner join cuenta on(codigo_mayor=cuenta) inner join libro_diario on(detalle_libro_diario.id_movimiento=libro_diario.id_movimiento) where mes=".json_encode($mes)." and ano=".$ano." and er=3 group by cuenta order by tipo_cuenta ";
		$result5=mysql_query($query5) or die(mysql_error());
		while($r5=mysql_fetch_assoc($result5)){
			echo "
			<tr>
		
			<td>".$r5['nombre_cuenta']."</td>
			<td>$".$sHaber6."</td>
			<td>$".$r5['haber']."</td>
			</tr>";
			
			
			$sHaber5=$sHaber5+$r5['haber'];
		}

		$totalDebe=$sDebe+$sDebe2;
		$totalHaber=$sHaber2+$sHaber3+$sHaber5;

	echo "<br>";
	echo "<br>";
	echo "<tr>
			<th colspan='3'></th>
		  </tr>
		
		<tr>
		<td>totales</td>
		<td >$".$totalDebe."</td>
		<td >$".$totalHaber."</td>
		</tr>";
echo "</table>";
		}
		
?>
<a href="detalle_balance_pdf.php" class="btn btn-success btn-lg btn-block btn-raised">Generar Reporte</a>
			</div>
			
		</article> <!-- /article -->
	
	</section> <!-- / #main-content -->
 
	
	
	<footer id="main-footer">
		<p>&copy; 2016 <a href="http://FranciscoAMK.com">Universidad de El Salvador</a></p>
	</footer> <!-- / #main-footer -->
 <?php include("modal.php"); ?>

 <script src="../js/bootstrap.min.js"></script>
	
</body>
</html>