<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Sistema Contable</title>
 
        <link rel="stylesheet" type="text/css" href="../css/style.css"/>
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>
        <script>
        !window.jQuery && document.write("<script src='../js/jquery.min.js'><\/script>");
    </script>
</head>
 
<body>
	<header id="main-header">      
        <a id="logo-header" href="#">
            <span class="site-name">Sistema Contable</span>
            <span class="site-desc"></span>
        </a>
 
		<nav>     
            <ul>
            <?php
            echo "
            <li><a href=\"inicio.php\">Inicio</a></li>
            <li><a href=\"catalogo.php\">Catálogo de cuentas</a></li>
            <li><a href=\"libro_diario.php\">Libro Diario</a></li>
            <li><a href=\"balance_comprobacion.php\">Balance de Comprobación</a></li>
            <li><a href=\"estado_resultados.php\">Estado de Resultados</a></li>
            <li><a href=\"estado_capital.php\">Estado de Capital</a></li>
            <li><a href=\"balance_general.php\">Balance General</a></li>
            <li><a href=\"costos.php\">Área Costos</a></li>"
            ;
            ?>
            <li><a href="#logout" data-toggle="modal"><!-- <span class="glyphicon glyphicon-log-out"></span> --> &nbsp;Cerrar sesión</a></li>
            </ul>
        </nav><!-- / nav -->
 
	</header><!-- / #main-header -->
 
	
	<section id="main-content">
	
		<article>
			<header>
				
			</header>
			
			
			
			<div class="content">
			
<div class="row row-offcanvas row-offcanvas-right">
			<div class="page-header">
    			<h3>Registrar a un empleado</h3>
    		</div>
    		<div class="row">
                <?php 
                error_reporting(0);
                

                if ( isset( $_GET["result"] ) ) :
                    $error = $_GET["result"];

                    /**
                     * Si hay no error en la ejecución de la operación
                     * imprimir mensaje de exito.
                     */
                    if($error == "exito") {
                        echo "<div class='alert alert-success alert-dismissable'>";
                        echo "<button type='button' class='close' data-dismiss='alert'>&times;</button>";
                        echo "Se agregó al empleado correctamente.";
                        echo "</div>";
                    }

                    /**
                     * Si hay error en la ejecución de la operación,
                     * imprimir de igual forma el error.
                     */
                    else {
                        echo "<div class='alert alert-danger alert-dismissable'>";
                        echo "<button type='button' class='close' data-dismiss='alert'>&times;</button>";
                        echo $_GET["error"];
                        echo "</div>";
                    }
                endif;
                ?>

    			<div class="col-lg-12 well">
    				<form action="agregar-empleado.php" id="nuevo_empleado" name="nuevo_empleado_frm" method="post" class="form-horizontal" enctype="application/x-www-form-urlencoded" onSubmit="return validarEmpleado()">
    					<fieldset>
    						<legend>Datos del empleado</legend>
    						<div class="container">
    							<div class="row">
    								<div class="col-lg-4">
    									<label for="codigo_empleado" class="control-label">Código del empleado</label>
    									<input type="text" class="form-control" id="codigo_empleado" name="codigo_empleado_txt" title="Escriba el código único para el empleado" placeholder="Por ejemplo AA000001" required />
                                        
    								</div>
    							</div>
    							<br>
    							<div class="row">
    								<div class="col-lg-3">
    									<label for="primernombre" class="control-label">Primer Nombre</label>
    									<input type="text" id="primernombre" name="primernombre_txt" class="form-control" title="Escriba el primer nombre del empleado" placeholder="Primer nombre" required />
                                          
    								</div>
    								<div class="col-lg-3">
    									<label for="segundonombre" class="control-label">Segundo Nombre</label>
    									<input type="text" id="segundonombre" name="segundonombre_txt" class="form-control" title="Escriba el segundo nombre del empleado" placeholder="Segundo nombre" />
                                         
    								</div>
    								<div class="col-lg-3">
    									<label for="primerapellido" class="control-label">Primer Apellido</label>
    									<input type="text" id="primerapellido" name="primerapellido_txt" class="form-control" title="Escriba el primer apellido del empleado" placeholder="Primer nombre" required />
                                          
    								</div>
    								<div class="col-lg-3">
    									<label for="segundoapellido" class="control-label">Segundo Apellido</label>
    									<input type="text" id="segundoapellido" name="segundoapellido_txt" class="form-control" title="Escriba el segundo apellido del empleado" placeholder="Segundo apellido" />
                                        
    								</div>
    							</div>
    							<br>
    							<div class="row">
    								<div class="col-lg-4">
    									<label for="cargo" class="control-label">Cargo del empleado</label>
    									<select name="cargos_slc" id="cargo" class="form-control">
    										<option value="">Seleccione cargo</option>
    										<?php include("select-cargos.php"); ?>
    									</select>
    								</div>
    								<div class="col-lg-4">
    									<label for="aniostrabajados" class="control-label">Años trabajados</label>
    									<input type="number" min="1" max="70" class="form-control" id="aniostrabajados" name="aniostrabajados_txt" title="Digite los años de trabajo del empleado" placeholder="Años trabajados" required/>
                                        <p class="text-muted">entre 1 año y 70 años.</p>
    								</div>
    								<div class="col-lg-4">
    									<label for="salario" class="control-label">Salario contratado</label>
    									<div class="input-group">
    										<span class="input-group-addon">$</span>
    										<input type="text" id="salario" name="salario_txt" class="form-control" title="Escriba el salario del empleado" placeholder="Salario base" required />
                                        </div>
                                       
    								</div>
    							</div>
    							<br>
    							<div class="row">
    								<div class="col-lg-12">
    									<button class="btn btn-primary pull-right" type="submit"><span class="glyphicon glyphicon-save"></span>&nbsp; Guardar</button>
    								</div>
    							</div>
    						</div>
    					</fieldset>
    				</form>
    			</div>
    		</div>
        	
        </div>



			</div>


			
		</article> <!-- /article -->
	
	</section> <!-- / #main-content -->
 
	
	
	<footer id="main-footer">
		<p>&copy; 2016 <a href="http://FranciscoAMK.com">Universidad de El Salvador</a></p>
	</footer> <!-- / #main-footer -->
 <?php include("modal.php"); ?>

 <script src="../js/bootstrap.min.js"></script>
	
</body>
</html>