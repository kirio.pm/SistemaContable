<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Sistema Contable</title>
     <link rel="stylesheet" href="../css/style.css">
     <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css"/>
       <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>
     <script src="../js/jquery-3.1.1.js"></script>
       <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script src="../js/libro.js"></script>
    <script src="../js/libro_compraventa.js"></script>
    <script>
	    !window.jQuery && document.write("<script src='../js/jquery.min.js'><\/script>");
	</script>
   </head>
 
<body>
	<header id="main-header">		
		<a id="logo-header" href="#">
			<span class="site-name">Sistema Contable</span>
			<span class="site-desc"></span>
		</a>

		<nav>		
			<ul>
			<?php
			echo "
			<li><a href=\"inicio.php\">Inicio</a></li>
            <li><a href=\"catalogo.php\">Catálogo de Cuentas</a></li>
            <li><a href=\"libro_diario.php\">Libro Diario</a></li>
            <li><a href=\"balance_comprobacion.php\">Balance de Comprobación</a></li>
            <li><a href=\"estado_resultados.php\">Estado de Resultados</a></li>
            <li><a href=\"estado_capital.php\">Estado de Capital</a></li>
            <li><a href=\"balance_general.php\">Balance General</a></li>
            <li><a href=\"costos.php\">Área Costos</a></li>"
			;
			?>
			<li><a href="#logout" data-toggle="modal"><!-- <span class="glyphicon glyphicon-log-out"></span> --> &nbsp;Cerrar sesión</a></li>
			</ul>
		</nav><!-- / nav -->
 
	</header><!-- / #main-header -->
 
	
	<section id="main-content">
	
		<article>
			<header>
				<h1>Registro Diario</h1>
			</header>
			
			<div class="content">
            <div id='validarCuentasActivo'></div>
          <div id='validarPartidaDoble'></div>
<form name="libro_diario" id="formPrincipal" method="post" action="registrar_diario.php">
<table>
<tr>
<td colspan="3">Partida N°: <input name="codigo_libro_diario" type="text" value="<?php include 'diariodb.php'; $prox_part=get_partidaN()+1;echo $prox_part ?>" size="3" required></td>
</tr>
<tr>
<td width="20%">
Dia: &nbsp;<select name="dia" id="select_dia"></select>
</td>
<td width="30%">
Mes: &nbsp;<select name="mes" id="select_mes"></select>
</td>
<td width="50%">
Año: &nbsp;<select name="anio" id="select_anio"></select>
</td>
</tr>
</table>
<table border="1">
<tr>
<th>Cuenta</th>
<th>Deber</th>
<th>Haber</th>
</tr>
<?php
if(isset($_GET['u'])){
$u=$_GET['u'];	
	}
else {
	$u=2;
	}
for($i=0;$i<$u;$i++){
imprimir_celdas_cuentas();
}

echo "<tr><td><b><a href='".$_SERVER['PHP_SELF']."?u=".($u+1)."'>+Añadir otro campo</a></b></td> <td ><b><a href='".$_SERVER['PHP_SELF']."?u=".($u-1)."'>-Eliminar Ultimo campo</a></b></td></tr></table>";
?>

<br>
<table id="iva" width="93%" border="1">
  <tr>
    <th width="50%" scope="col">Cuenta IVA</th>
    <th width="25%" scope="col">DEBE</th>
    <th width="25%" scope="col">HABER</th>
  </tr>
  <tr>
    <td > <select name='cuenta[]' class="cuenta">
    <?php
	agregar_selectIVA();
	?>
    </select></td>
    <td><input type="text" name='deber[]' class="debe" size="10" value=0  onchange='verificarPartidaDoble()'></td>
    <td><input type="text" name='haber[]'  class="haber" size="10" value=0  onchange='verificarPartidaDoble()'></td>
  </tr>
</table>
<br>
Descripción:<br> 
<textarea name="descripcion" rows="5" cols="70"></textarea><br>
<input type="hidden" value="<?php echo $u; ?>" name="n">
<br>
<input class="btn btn-lg btn-primary btn-block" type="button" id='agregar' value="Agregar Iva">
<input class="btn btn-lg btn-primary btn-block" type="button" id='enviar' value="Registrar">
<input class="btn btn-lg btn-primary btn-block" type="reset" value="Limpiar Formulario">
</form>
			</div>
			
		</article> <!-- /article -->
	
	</section> <!-- / #main-content -->
	
	<footer id="main-footer">
		<p>&copy; 2016 <a href="http://FranciscoAMK.com">Universidad de El Salvador</a></p>
	</footer> <!-- / #main-footer -->
<?php include("modal.php"); ?>

 <script src="../js/bootstrap.min.js"></script>
</body>
<select 
</html>