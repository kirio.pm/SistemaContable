
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Sistema Contable</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	<script>
	    !window.jQuery && document.write("<script src='../js/jquery.min.js'><\/script>");
	</script>
 
 
        <link rel="stylesheet" type="text/css" href="../css/style.css"/>
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>
         <script src="../jquery-ui/external/jquery/jquery.js"></script>
		 <script src="../jquery-ui/jquery-ui.js"></script>
		 <script src="../js/ordenScript.js"></script>
	
</head>
 
<body>
	<header id="main-header">		
		<a id="logo-header" href="#">
			<span class="site-name">Sistema Contable</span>
			<span class="site-desc"></span>
		</a>

		<nav>		
			<ul>
			<?php
			echo "
			<li><a href=\"inicio.php\">Inicio</a></li>
            <li><a href=\"catalogo.php\">Catálogo de cuentas</a></li>
            <li><a href=\"libro_diario.php\">Libro Diario</a></li>
            <li><a href=\"balance_comprobacion.php\">Balance de Comprobación</a></li>
            <li><a href=\"estado_resultados.php\">Estado de Resultados</a></li>
            <li><a href=\"estado_capital.php\">Estado de Capital</a></li>
            <li><a href=\"balance_general.php\">Balance General</a></li>
            <li><a href=\"costos.php\">Área Costos</a></li>"
			;
			?>
			<li><a href="#logout" data-toggle="modal"><!-- <span class="glyphicon glyphicon-log-out"></span> --> &nbsp;Cerrar sesión</a></li>
			</ul>
		</nav><!-- / nav -->
 
	</header><!-- / #main-header -->
			
	<br>
	<br>		
	<div class="content">
	<h1>Ordenes de fabricacion</h1>
		<button id="agregar" class="btn btn-success btn-lg btn-block btn-raised">Agregar una Nueva Orden</button>
<!-- ui-dialog -->
<div id="dialog" title="Orden">
	<form>
		<label>Numero de orden</label><input type="number" name="numOrden" id="ordenNum">
	</form>
</div>
	<br>
		
<table id="tablaOrdenes" >
	
	
		<thead id="encabezado">
			
		</thead>	
			
		<tbody>
			<tr id="tr1">
				<td>Inventario Inicial</td>
			</tr>
			<tr id="tr2">
				<td>+ Compras</td>
			</tr>
			<tr id="tr3">
				<td>=MP disponible</td>
			</tr>

			<tr id="tr4">
				<td>-inventario final</td>
			</tr>
			<tr id="tr5">
				<td>=MP utilizada</td>
			</tr>
			<tr id="tr6">
				<td>+Inv. Inici de Prod en proc</td>
			</tr>
			<tr id="tr7">
				<td>+MOD</td>
			</tr>
			<tr id="tr8">
				<td>+CIF</td>
			</tr>
			<tr id="tr9">
				<td>-inv. final de Prod proceso </td>
			</tr>
			<tr id="tr10">
				<td>=Costo art.terminado</td>
			</tr>
			<tr id="tr11">
				<td>+inv inicial de prod. terminado</td>
			</tr>
			<tr id="tr12">
				<td>=art. terminados disponibles</td>
			</tr>
			<tr id="tr13">
				<td>-inv final de producto terminado</td>
			</tr>
			<tr id="tr14">
				<td>=Costo de lo vendido</td>

			</tr>
		</tbody>
	</table> 


			</div>
	
	
 	
	<footer id="main-footer">
		<p>&copy; 2016 <a href="http://FranciscoAMK.com">Universidad de El Salvador</a></p>
	</footer> <!-- / #main-footer -->
	<?php include("modal.php"); ?>

 <script src="../js/bootstrap.min.js"></script>
</body>
</html>