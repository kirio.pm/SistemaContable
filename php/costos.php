
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Sistema Contable</title>
	<script>
	    !window.jQuery && document.write("<script src='../js/jquery.min.js'><\/script>");
	</script>
 
 
        <link rel="stylesheet" type="text/css" href="../css/style.css"/>
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>
</head>
 
<body>
	<header id="main-header">		
		<a id="logo-header" href="#">
			<span class="site-name">Sistema Contable</span>
			<span class="site-desc"></span>
		</a>

		<nav>		
			<ul>
			<?php
			echo "
			<li><a href=\"inicio.php\">Inicio</a></li>
            <li><a href=\"catalogo.php\">Catálogo de cuentas</a></li>
            <li><a href=\"libro_diario.php\">Libro Diario</a></li>
            <li><a href=\"balance_comprobacion.php\">Balance de Comprobación</a></li>
            <li><a href=\"estado_resultados.php\">Estado de Resultados</a></li>
            <li><a href=\"estado_capital.php\">Estado de Capital</a></li>
            <li><a href=\"balance_general.php\">Balance General</a></li>
            <li><a href=\"costos.php\">Área Costos</a></li>"
			;
			?>
			<li><a href="#logout" data-toggle="modal"><!-- <span class="glyphicon glyphicon-log-out"></span> --> &nbsp;Cerrar sesión</a></li>
			</ul>
		</nav><!-- / nav -->
 
	</header><!-- / #main-header -->
	
	<section id="main-content">
	
		<article>
			<header>
			</header>
			<h1>Costabilidad de costos</h1>

			<!--  <div class="btn-group">
					  <button type="button" class="btn btn-primary"><a href="../php/alta-empleado.php">Registrar Empleado</a></button><br><br>
					  <button  type="button" class="btn btn-primary"><li><a href="../php/ver-planilla.php">Ver planilla</a></li></button><br><br>
					  <button type="button" class="btn btn-primary"><li><a href="../php/index2.php">Kardex</a></li></button><br><br>
					  <button type="button" class="btn btn-primary"><li><a href="../php/orden_de_fabricacion.php">Orden De Fabricación</a></li></button><br><br>
			</div> -->
						
			<center> <div class="content">

				<div class="panel panel-primary">
		<a href="../php/alta-empleado.php" class="btn btn-success btn-lg btn-block btn-raised"> Registrar Empleado</a>
	</div>
	<div class="panel panel-primary">
		<a href="../php/ver-planilla.php" class="btn btn-success btn-lg btn-block btn-raised">Ver planilla</a>
	</div>
	<div class="panel panel-primary">
		<a href="../php/kardex.php" class="btn btn-success btn-lg btn-block btn-raised">Kardex</a>
	</div>
	<div class="panel panel-primary">
		<a href="../php/orden_de_fabricacion.php" class="btn btn-success btn-lg btn-block btn-raised">Orden De Fabriacion</a>
	</div>
				<!-- <div class="panel panel-primary">
					<li><a href="../php/EstadoDeCostos.php">Estado de Costos</a></li>
				</div> -->
			</div>
			</center>
		</article> <!-- /article -->
	
	</section> <!-- / #main-content -->
 	
	<footer id="main-footer">
		<p>&copy; 2016 <a href="http://FranciscoAMK.com">Universidad de El Salvador</a></p>
	</footer> <!-- / #main-footer -->
 <?php include("modal.php"); ?>

 <script src="../js/bootstrap.min.js"></script>
	
</body>
</html>