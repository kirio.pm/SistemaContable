
<!DOCTYPE html>
<html>
<head>
	<title>Kardex</title>
	<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
	<meta charset="utf-8">
	<link href="../jquery-ui/jquery-ui.css" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="../css/style.css"/>
	<link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css"/>
    <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>
    <script>
	    !window.jQuery && document.write("<script src='../js/jquery.min.js'><\/script>");
	</script>
 

</head>
<body>
<header id="main-header">		
		<a id="logo-header" href="#">
			<span class="site-name">Sistema Contable</span>
			<span class="site-desc"></span>
		</a>

		<nav>		
			<ul>
			<?php
			echo "
			<li><a href=\"inicio.php\">Inicio</a></li>
            <li><a href=\"catalogo.php\">Catálogo de cuentas</a></li>
            <li><a href=\"libro_diario.php\">Libro Diario</a></li>
            <li><a href=\"balance_comprobacion.php\">Balance de Comprobación</a></li>
            <li><a href=\"estado_resultados.php\">Estado de Resultados</a></li>
            <li><a href=\"estado_capital.php\">Estado de Capital</a></li>
            <li><a href=\"balance_general.php\">Balance General</a></li>
            <li><a href=\"costos.php\">Área Costos</a></li>"
			;
			?>
			<li><a href="#logout" data-toggle="modal"><!-- <span class="glyphicon glyphicon-log-out"></span> --> &nbsp;Cerrar sesión</a></li>
			</ul>
		</nav><!-- / nav -->
 
	</header><!-- / #main-header -->

<table width="100%" cellspacing="0" id="kardex">
	<thead>
		<th class="tituloColumna">Entradas</th>
		<th class="tituloColumna">Salidas</th>
		<th class="tituloColumna">Saldos</th>
	</thead>
	<tbody>
		<tr>
			<td class="entrada">
				<table cellpadding="0" id="tablaInternaEntrada">
					<thead>
						<th>Cantidad</th>
						<th>Precio Unitario</th>
						<th>Monto</th>
					</thead>
					<tbody>		<!--Donde se agrega cada cantidad salida y monto de las 				entradas-->

					</tbody>
				</table>
			</td>
			<td class="salida">
				
				<table cellpadding="0" id="tablaInternaSalida">
					<thead>
						<th>Cantidad</th>
						<th>Precio Unitario</th>
						<th>Monto</th>
					</thead>
					<tbody>
						
					</tbody>
				</table>

			</td>

			<td class="saldo">
				
				<table cellpadding="0" id="tablaInternaSaldo">
					<thead>
						<th>Cantidad</th>
						<th>Precio Unitario</th>
						<th>Monto</th>
					</thead>
					<tbody></tbody>
				</table>

			</td>
		</tr>


	</tbody>
</table>

<div > <output id="resultado"></output></div>


<div>

	<button id="agregarEntrada" class="ui-button ui-corner-all ui-widget">
		<span class="ui-icon ui-icon-newwin"></span>Agregar Entrada
	</button>

<button id="agregarSalida" class="ui-button ui-corner-all ui-widget">
<span class="ui-icon ui-icon-newwin"></span>Agregar Salida
</button>

<button id="cerrarPeriodo" class="ui-button ui-corner-all ui-widget">
		<span class="ui-icon ui-icon-newwin"></span>Cerrar Periodo
	</button>

	
</div>




<!-- ui-dialog -->
<div id="formularioEntrada" title="Entrada de producto">
	<form  >
		<br><label>Cantidad</label> <input type="number" name="cantidadProducto" required="true" id="cantidadEntrada">
		<br><br><label>Precio Unitario</label><input type="number" name="precioUnitario" required="true" id="precioEntrada" value="">
		<br><br><label>Monto:</label><input id="montoEntrada" name="montoEntrada" disabled="true">
		<!--<input type="submit" name="ok" id="btnEnviar" > -->
	</form>
</div>

<div id="formularioSalida" title="Salida de producto">
	<form  >
		<br><label>Cantidad</label> <input type="number" name="cantidadProducto" required="true" id="cantidadSalida"><br>				
	</form>
</div>

<div id="formularioCierrePeriodo" title="Cierre de periodo">
	<form  >
		<br><label>Ingreso por venta</label> <input type="number" name="IngresoPorVenta" required="true" id="ingresoPorVenta"  >
		<br><br><label>Porcentaje de renta (Ejemplo: 0.25)</label><input type="number" name="renta" id="renta" value="" disabled="true">
		<br><br><label>Gasto de Operación</label><input id="gastoDeOperacion" name="GastoOperacion">
		<br><br><label>Gasto de Venta</label><input type="number" name="GastoDeVenta" id="gastoDeVenta" value="">
		<br><br><label>Gastos Financieros</label><input type="number" name="GastosFinancieros" id="gastosFinancieros" value="">
		<br><br><label>Otros Ingresos </label><input type="number" name="otrosIngresos" id="otrosIngresos" value="">
		<!--<input type="submit" name="ok" id="btnEnviar" > -->
	</form>

</div>
<?php include("modal.php"); ?>

 <script src="../js/bootstrap.min.js"></script>

</body>

	<script src="../jquery-ui/external/jquery/jquery.js"></script>
	<script src="../jquery-ui/jquery-ui.js"></script>
	<script src="../js/Scripts.js"></script>
	
</html>