<?php

require_once("../dompdf/dompdf_config.inc.php");
include('conexion.php');
include('sesion.php');

$codigoHTML='
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" type="text/css" href="../css/stylePdf.css"/>
<title>Documento sin título</title>
</head>
<body>
<h1>Estado De Capital</h1>
<h2>Panadería Freshbread</h2>
<h2>Del 1 de '.$_SESSION['mes2'].' al 30 del año '.$_SESSION['anio2'].' </h2>

<table border=1>
  <tr>
<th>Cuenta</th>
<th>Deber</th>
<th>Haber</th>
</tr>';

		$debe=0;
		$habe=0;
		$sDebe=0;
		$sHaber=0;
		$CPtotal;
		$UtilidadP=array('utilidad(+)/perdida(-)');
		$array=$UtilidadP;
		

$codigoHTML.='

		<tr>
		<th colspan="3">Inversiones</th>
		</tr>
		<tr>
		<td>utilidad(+)/perdida(-)</td>
		<td>'.$debe.'</td>
		<td>'.$_SESSION['resultado'].'</td>
		</tr';		
		$sHaber2=$_SESSION['resultado'];

		//Inversion

		$query="select nombre_cuenta,sum(deber) as deber,sum(haber) as haber from detalle_libro_diario inner join cuenta on(codigo_mayor=cuenta) inner join libro_diario on(detalle_libro_diario.id_movimiento=libro_diario.id_movimiento) where mes=". "'". $_SESSION['mes2']. "'" ." and ano=".$_SESSION['anio2']." and er=3 group by cuenta order by tipo_cuenta ";
		$result=mysql_query($query) or die(mysql_error());
		while($r=mysql_fetch_assoc($result)){

$codigoHTML.='

		<tr>
		<td>'.$r['nombre_cuenta'].'</td>
		<td>$'.$debe.'</td>
		<td>$'.$r['haber'].'</td>
		</tr>
		';
		$sHaber=$sHaber+$r['haber'];
		}
		$CPtotal=$sHaber+$sHaber2;

$codigoHTML.='
		<tr>
		<th colspan="3">Desinversiones</th>
		</tr>
		<tr>
		<td>gastos por robo</td>
		<td>$0</td>
		<td>$0</td>
		</tr>		
		
		<tr>
		<th colspan="3">Total</th>
		</tr>
		<tr>
		<td>capital social</td>
		<td colspan="2">$'.$CPtotal.'</td>
		</tr>
</table>
</body>
</html>';
		//$codigoHTML=utf8_decode(utf8_encode($codigoHTML));
		$dompdf=new DOMPDF();
		$dompdf->load_html($codigoHTML);
		ini_set("memory_limit","128M");
		$dompdf->render();
		$dompdf->stream("Reporte_Estado_Capital.pdf");
?>