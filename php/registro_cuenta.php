<?php
include('conexion.php');

$tipos=mysql_query("select id_tipo_cuenta,nombre_tipo_cuenta from tipo_cuenta") or die(mysql_error());
?>

<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Sistema Contable</title>
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
 	
 	<script>
	    !window.jQuery && document.write("<script src='../js/jquery.min.js'><\/script>");
	</script>
        <link rel="stylesheet" type="text/css" href="../css/style.css"/>
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>
</head>
 
<body>
	<header id="main-header">		
		<a id="logo-header" href="#">
			<span class="site-name">Sistema Contable</span>
			<span class="site-desc"></span>
		</a>

		<nav>		
			<ul>
			<?php
			echo "
			<li><a href=\"inicio.php\">Inicio</a></li>
            <li><a href=\"catalogo.php\">Catálogo de Cuentas</a></li>
            <li><a href=\"libro_diario.php\">Libro Diario</a></li>
            <li><a href=\"balance_comprobacion.php\">Balance de Comprobación</a></li>
            <li><a href=\"estado_resultados.php\">Estado de Resultados</a></li>
            <li><a href=\"estado_capital.php\">Estado de Capital</a></li>
            <li><a href=\"balance_general.php\">Balance General</a></li>
            <li><a href=\"costos.php\">Área Costos</a></li>"
			;
			?>
			<li><a href="#logout" data-toggle="modal"><!-- <span class="glyphicon glyphicon-log-out"></span> --> &nbsp;Cerrar sesión</a></li>
			</ul>
		</nav><!-- / nav -->
 
	</header><!-- / #main-header -->
 
	
	<section id="main-content">
	
		<article>
			<header>
				<h1>Registro</h1>
			</header>
				
			<div class="content">
			<form name="registro_cuentas" action="registrar_cuenta.php" method="post">
<div id="table-content">
<table border="1" >
<tr>
<th>Tipo de Cuenta</th>
<th>Código de Mayor </th>
<th>Nombre de Cuenta</th>
<th> Descripción </th>
</tr>
<tr>
<td>
<?php
echo "<select name=\"tipo_cuenta\" >";
while($res=mysql_fetch_assoc($tipos)) {
echo "<option value=\"".$res['id_tipo_cuenta']."\" >".$res['nombre_tipo_cuenta']."</option>";
};
echo "</select>"; 
?>
</td>
<td><input type="text" size="10" name="codigo_mayor" id="active"  autocomplete="off" required></td>
<td><input type="text" size="25px" name="nombre_cuenta" id="active"  autocomplete="off" required></td>

<td><textarea name="descripcion_cuenta" id="active"  autocomplete="off" required></textarea></td>
</tr>
</table>

<input class="btn btn-lg btn-primary btn-block" type="submit" value="Registrar"  >
</form>
</div>

				
			</div>
			
		</article> <!-- /article -->
	
	</section> <!-- / #main-content -->
 
	
	
	<footer id="main-footer">
		<p>&copy; 2016 <a href="http://FranciscoAMK.com">Universidad de El Salvador</a></p>
	</footer> <!-- / #main-footer -->
 <?php include("modal.php"); ?>

 <script src="../js/bootstrap.min.js"></script>
	
</body>
</html>
