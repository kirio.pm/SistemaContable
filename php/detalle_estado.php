
<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Sistema Contable</title>
 	<script>
	    !window.jQuery && document.write("<script src='../js/jquery.min.js'><\/script>");
	</script>
        <link rel="stylesheet" type="text/css" href="../css/style.css"/>
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>
</head>
 
<body>
	
	<header id="main-header">		
		<a id="logo-header" href="#">
			<span class="site-name">Sistema Contable</span>
			<span class="site-desc"></span>
		</a>

		<nav>		
			<ul>
			<?php
			echo "
			<li><a href=\"inicio.php\">Inicio</a></li>
            <li><a href=\"catalogo.php\">Catálogo de cuentas</a></li>
            <li><a href=\"libro_diario.php\">Libro Diario</a></li>
            <li><a href=\"balance_comprobacion.php\">Balance de Comprobación</a></li>
            <li><a href=\"estado_resultados.php\">Estado de Resultados</a></li>
            <li><a href=\"estado_capital.php\">Estado de Capital</a></li>
            <li><a href=\"balance_general.php\">Balance General</a></li>
            <li><a href=\"costos.php\">Área Costos</a></li>"
			;
			?>
			<li><a href="#logout" data-toggle="modal"><!-- <span class="glyphicon glyphicon-log-out"></span> --> &nbsp;Cerrar sesión</a></li>
			</ul>
		</nav><!-- / nav -->
 
	</header><!-- / #main-header -->
 
	
	<section id="main-content">
	
		<article>
			<header>
				
			</header>
			
		<H1>Estado de Resultado</H1>
			
			<div class="content">
			<br>
<?php
include('conexion.php');
include('sesion.php');

if((!isset($_GET['m']))&&(!isset($_GET['a']))){
	header('location:balance_comprobacion.php');	
	}
	else{
		$mes=$_GET['m'];
		$ano=$_GET['a'];
		$_SESSION['mes1']=$mes;
		$_SESSION['anio1']=$ano;
		$query="select nombre_cuenta,sum(deber) as deber,sum(haber) as haber from detalle_libro_diario inner join cuenta on(codigo_mayor=cuenta) inner join libro_diario on(detalle_libro_diario.id_movimiento=libro_diario.id_movimiento) where mes=".json_encode($mes)." and ano=".$ano." and er=1 group by cuenta order by tipo_cuenta ";
		$result=mysql_query($query) or die(mysql_error());
		$debe=0;
		$habe=0;
		$sDebe=0;
		$sHaber=0;
		$UNtotal=0;
		
		echo "<h4>Estado de resultado del mes de ".$mes." del año ".$ano."</h4>";
		echo "<table border=1>
		<tr>
		<th>Cuenta</th>
		<th>Deber</th>
		<th>Haber</th>
		</tr>";
		$query2="select nombre_cuenta,sum(deber) as deber,sum(haber) as haber from detalle_libro_diario inner join cuenta on(codigo_mayor=cuenta) inner join libro_diario on(detalle_libro_diario.id_movimiento=libro_diario.id_movimiento) where mes=".json_encode($mes)." and ano=".$ano." and nombre_cuenta ='Ingreso por venta' and er=0 group by cuenta order by tipo_cuenta ";
		$result2=mysql_query($query2) or die(mysql_error());
		$r2=mysql_fetch_assoc($result2);
		echo "<tr>
		<th colspan='3'>Ingresos</th>
		</tr>
		<tr>
		<td>".$r2['nombre_cuenta']."</td>
		<td>$".$r2['deber']."</td>
		<td>$".$r2['haber']."</td>
		</tr>
		
		<tr> <th colspan='3'>Egresos</th></tr>";
		$sHaber2=$r2['haber'];

		while($r=mysql_fetch_assoc($result)){
			echo "<tr>

			<td>".$r['nombre_cuenta']."</td>
			<td>$".$r['deber']."</td>
			<td>$".$r['haber']."</td>
			</tr>";
			
			$sDebe=$sDebe+$r['deber'];
			$sHaber=$sHaber+$r['haber'];
		}

		$UNtotal=$sHaber2-$sDebe-$sHaber;
		$_SESSION['resultado']=$UNtotal; 
		echo "
		<tr>
		<th></th>
		<th colspan='2'>Total</th>
		</tr>
		<tr>
		<td>Utilidades Netas(+)/Perdidas(-)</td>
		<td colspan='2'>$".$UNtotal."</td>
		</tr>";
		echo "</table>";
	}
?>






				<a href="detalle_estado_pdf.php" class="btn btn-success btn-lg btn-block btn-raised">Generar Reporte</a>
			
			</div>
			
		</article> <!-- /article -->
	
	</section> <!-- / #main-content -->
 
	
	
	<footer id="main-footer">
		<p>&copy; 2016 <a href="http://FranciscoAMK.com">Universidad de El Salvador</a></p>
	</footer> <!-- / #main-footer -->
  <?php include("modal.php"); ?>

 <script src="../js/bootstrap.min.js"></script>
	
</body>
</html>