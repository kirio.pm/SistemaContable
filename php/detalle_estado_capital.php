<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="utf-8">
	<title>Sistema Contable</title>
 
        <link rel="stylesheet" type="text/css" href="../css/style.css"/>
        <link rel="stylesheet" type="text/css" href="../css/bootstrap.min.css"/>
        <link rel="stylesheet" type="text/css" href="../css/estilos.css"/>
        <script>
	    !window.jQuery && document.write("<script src='../js/jquery.min.js'><\/script>");
	</script>
</head>
 
<body>
	
	<header id="main-header">		
		<a id="logo-header" href="#">
			<span class="site-name">Sistema Contable</span>
			<span class="site-desc"></span>
		</a>

		<nav>		
			<ul>
			<?php
			echo "
			<li><a href=\"inicio.php\">Inicio</a></li>
            <li><a href=\"catalogo.php\">Catálogo de cuentas</a></li>
            <li><a href=\"libro_diario.php\">Libro Diario</a></li>
            <li><a href=\"balance_comprobacion.php\">Balance de Comprobación</a></li>
            <li><a href=\"estado_resultados.php\">Estado de Resultados</a></li>
            <li><a href=\"estado_capital.php\">Estado de Capital</a></li>
            <li><a href=\"balance_general.php\">Balance General</a></li>
            <li><a href=\"costos.php\">Área Costos</a></li>"
			;
			?>
			<li><a href="#logout" data-toggle="modal"><!-- <span class="glyphicon glyphicon-log-out"></span> --> &nbsp;Cerrar sesión</a></li>
			</ul>
		</nav><!-- / nav -->
 
	</header><!-- / #main-header -->
 
	
	<section id="main-content">
	
		<article>
			<header>
				
			</header>
			
		<h1>Estado de Capital</h1>
			
			<div class="content">
<?php
include('conexion.php');
include('sesion.php');

if((!isset($_GET['m']))&&(!isset($_GET['a']))){
	header('location:balance_comprobacion.php');	
	}
	else{
		$mes=$_GET['m'];
		$ano=$_GET['a'];
		$_SESSION['mes2']=$mes;
		$_SESSION['anio2']=$ano;
		$query="select nombre_cuenta,sum(deber) as deber,sum(haber) as haber from detalle_libro_diario inner join cuenta on(codigo_mayor=cuenta) inner join libro_diario on(detalle_libro_diario.id_movimiento=libro_diario.id_movimiento) where mes=".json_encode($mes)." and ano=".$ano." and er=3 group by cuenta order by tipo_cuenta ";
		$result=mysql_query($query) or die(mysql_error());
		$debe=0;
		$habe=0;
		$sDebe=0;
		$sHaber=0;
		$CPtotal=0;
		$UtilidadP=array('utilidad(+)/perdida(-)');
		$array=$UtilidadP;
		
		echo "<h4>Estado de capital del mes de ".$mes." del año ".$ano."</h4>";
		echo "<table border=1>
		<tr>
		<th>Cuenta</th>
		<th>Deber</th>
		<th>Haber</th>
		</tr>";
		$query2="select nombre_cuenta,sum(deber) as deber,sum(haber) as haber from detalle_libro_diario inner join cuenta on(codigo_mayor=cuenta) inner join libro_diario on(detalle_libro_diario.id_movimiento=libro_diario.id_movimiento) where mes=".json_encode($mes)." and ano=".$ano." and nombre_cuenta ='Ingreso por venta' and er=0 group by cuenta order by tipo_cuenta ";
		$result2=mysql_query($query2) or die(mysql_error());
		$r2=mysql_fetch_assoc($result2);
		
		echo "<br><tr>
		<th colspan='3'>Inversiones</th>
		</tr>
		<tr>
		<td>".$array[0]."</td>
		<td>$".$r2['deber']."</td>
		<td>$".$_SESSION['resultado']."</td>
		</tr>";
		$sHaber2=$_SESSION['resultado'];
		
		while($r=mysql_fetch_assoc($result)){
			echo "<tr>
			<td>".$r['nombre_cuenta']."</td>
			<td>$".$r['deber']."</td>
			<td>$".$r['haber']."</td>
			</tr>";
			
			$sDebe=$sDebe+$r['deber'];
			$sHaber=$sHaber+$r['haber'];
		}
		echo "<tr>
		<th>Desinversiones</th>
		</tr><tr>";
		$CPtotal=$sDebe+$sHaber+$sHaber2;
		$_SESSION['capital']=$CPtotal;
		
		echo "<tr>
		<th>nombre</th>
		<th colspan='2'>Total</th>
		</tr>
		<tr>
		<td>capital social</td>
		<td colspan='2'>$".$CPtotal."</td>
		</tr>";
		echo "</table>";
	}
?>



<a href="detalle_estado_capital_pdf.php" class="btn btn-success btn-lg btn-block btn-raised">Generar Reporte</a>



				
			</div>
			
		</article> <!-- /article -->
	
	</section> <!-- / #main-content -->
 
	
	
	<footer id="main-footer">
		<p>&copy; 2016 <a href="http://FranciscoAMK.com">Universidad de El Salvador</a></p>
	</footer> <!-- / #main-footer -->
  <?php include("modal.php"); ?>

 <script src="../js/bootstrap.min.js"></script>
	
</body>
</html>