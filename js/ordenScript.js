$(document).ready(function(){
	
	$( "#agregar" ).click(function( event ) {
	$( "#dialog" ).dialog( "open" );

	event.preventDefault();

});


	$( "#dialog" ).dialog({
	autoOpen: false,
	width: 400, 
	buttons: [
		{
			text: "Ok",
			click: function() {
				if ($("#ordenNum").val()<=0) {
					alert("Debe ingresar un numero positivo");
				}else{
					agregarColum();
				$( this ).dialog( "close" );
				}
				
				
			}
		},
		{
			text: "Cancel",
			click: function() {

				$( this ).dialog( "close" );
			}
		}
	]
});


});






function  agregarColum() {
	var i=0, numOrden;
	numOrden=$("#ordenNum").val();
	
	if ($("#encabezado>th").length==0) {
		$("#encabezado").append("<th></th>");
		$("#encabezado").append("<th >Orden de fabricacion "+numOrden+" </th>");
	}else{
		$("#encabezado").append("<th>Orden de fabricacion "+numOrden+" </th>");
	}		
	
	$("table>tbody>tr").each(function(index) {
		var id=numOrden+index;

		$(this).append("<td><input type='number' min='0' id='"+id+"'></td>");
		//alert(index);
		switch(index){

 			case 0:
 			var idBusqueda="#"+id;
			$(idBusqueda).attr({
				'onkeyup': "calcularMP("+numOrden+")",
				//property2: 'value2'
			});
			break;				
			
			case 1:
 			var idBusqueda="#"+id;
			$(idBusqueda).attr({
				'onkeyup': "calcularMP("+numOrden+")",
				//property2: 'value2'
			});			
			break;
			
			case 2:
			var idBusqueda="#"+id;
			
			$(idBusqueda).attr({
				'disabled': 'true',
				//property2: 'value2'
			});
			break;

			case 3:
			var idBusqueda="#"+id;
			$(idBusqueda).attr({
				'onkeyup': "calcularMPUsada("+numOrden+")",
				//property2: 'value2'
			});	
			break;

			case 4:
			var idBusqueda="#"+id;
			
			$(idBusqueda).attr({
				'disabled': 'true',
				//property2: 'value2'
			});

			break;


			case 5:
			var idBusqueda="#"+id;
			$(idBusqueda).attr({
				'onkeyup': "calcularCostoArtTerm("+numOrden+")",
				//property2: 'value2'
			});	
			break;
			case 6:
			var idBusqueda="#"+id;
			$(idBusqueda).attr({
				'onkeyup': "calcularCostoArtTerm("+numOrden+")",
				//property2: 'value2'
			});
 			break;

			case 7:
			var idBusqueda="#"+id;
			$(idBusqueda).attr({
				'onkeyup': "calcularCostoArtTerm("+numOrden+")",
				//property2: 'value2'
			});
 			break;

			case 8:
			var idBusqueda="#"+id;
			$(idBusqueda).attr({
				'onkeyup': "calcularCostoArtTerm("+numOrden+")",
				//property2: 'value2'
			});
 			break;

			
			case 9:
			var idBusqueda="#"+id;
			
			$(idBusqueda).attr({
				'disabled': 'true',
				//property2: 'value2'
			});
			case 10:
			var idBusqueda="#"+id;
			$(idBusqueda).attr({
				'onkeyup': "calcularArtTermDisponibles("+numOrden+")",
				//property2: 'value2'
			});
			break;

			break;
			case 11:
			var idBusqueda="#"+id;
			
			$(idBusqueda).attr({
				'disabled': 'true',
				//property2: 'value2'
			});
			break;
			case 12:
			var idBusqueda="#"+id;
			$(idBusqueda).attr({
				'onkeyup': "calcularCostoDeLoVendido("+numOrden+")",
				//property2: 'value2'
			});
			break;

			case 13:
			var idBusqueda="#"+id;
			
			$(idBusqueda).attr({
				'disabled': 'true',
				//property2: 'value2'
			});
			break;
			
		}
		
	});
	
	
}




function calcularMP(numeroOrden){
	var InvInic=0, compras=0, MP=0;
	InvInic=parseFloat($("#"+numeroOrden+0).val());
	compras=parseFloat($("#"+numeroOrden+1).val());
	MP=InvInic+compras;
	$("#"+numeroOrden+2).val(MP);
	if (MP<=0) {
		alert("Resultado Negativo, revisar datos");
	}



}

function calcularMPUsada(numeroOrden){
var invFinal=0, MP=0, MPUsada=0;
	MP=parseFloat($("#"+numeroOrden+2).val());
	invFinal=parseFloat($("#"+numeroOrden+3).val());
	MPUsada=MP-invFinal;
	$("#"+numeroOrden+4).val(MPUsada);
	if (MPUsada<=0) {
		alert("Resultado Negativo, revisar datos");
	}

}

function calcularCostoArtTerm(numeroOrden){
	var MPUsada=0, InvInicProdProc=0, MOD=0, CIF=0, InvFinProdProc=0, CostoArtTerm=0;
	MPUsada=parseFloat($("#"+numeroOrden+4).val());
	InvInicProdProc=parseFloat($("#"+numeroOrden+5).val());
	MOD=parseFloat($("#"+numeroOrden+6).val());
	CIF=parseFloat($("#"+numeroOrden+7).val());
	InvFinProdProc=parseFloat($("#"+numeroOrden+8).val());
	CostoArtTerm=MPUsada+InvInicProdProc+MOD+CIF-InvFinProdProc;
	$("#"+numeroOrden+9).val(CostoArtTerm);
	if (CostoArtTerm<=0) {
		alert("Resultado Negativo, revisar datos");
	}

}

function calcularArtTermDisponibles(numeroOrden){
	var costoArtTerm, InvInicProdTerm, artTermDisp=0;
	CostoArtTerm=parseFloat($("#"+numeroOrden+9).val());
	InvInicProdTerm=parseFloat($("#"+numeroOrden+10).val());
	artTermDisp=CostoArtTerm+InvInicProdTerm;

	$("#"+numeroOrden+11).val(artTermDisp);
	if (artTermDisp<=0) {
		alert("Resultado Negativo, revisar datos");
	}
	

}
function calcularCostoDeLoVendido(numeroOrden){
	var artTermDisp=0, invFinalProdTerm=0, costoDeLoVendido=0;
	artTermDisp=parseFloat($("#"+numeroOrden+11).val());
	invFinalProdTerm=parseFloat($("#"+numeroOrden+12).val());
	costoDeLoVendido=artTermDisp - invFinalProdTerm;
	$("#"+numeroOrden+13).val(costoDeLoVendido);

	if (costoDeLoVendido<=0) {
		alert("Resultado Negativo, revisar datos");
	}
	
}

