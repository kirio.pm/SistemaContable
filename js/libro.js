var IVAinput;
var cuentas;
var debe_colm;
var haber_colm;
var is_partidaDoble;
var cuentasD=["110101","110102","110103","110201","110202","110203","110204","110205","220101","220102","210102"];
var meses =['Enero','Febrero','Marzo','Abril','Mayo','Junio','Julio','Agosto','Septiembre','Octubre','Noviembre','Diciembre'];
$(document).ready(function() {
            IVAinput = document.querySelectorAll("#iva input");
			cuentas = document.querySelectorAll("#formPrincipal .cuenta");
			debe_colm = document.querySelectorAll("#formPrincipal .debe");
			haber_colm = document.querySelectorAll("#formPrincipal .haber");
			
			$(".debe").on("change",verificar_partidaDoble);
			$(".haber").on("change",verificar_partidaDoble);
			
			$("#agregar").on("click",agregar_IVA);
			agregar_calendario();

        });
function verificar_partidaDoble(){
				var total_debe=0;
				var total_haber=0;
				for(var i=0;i<cuentas.length;i++){
					total_debe += parseFloat(debe_colm.item(i).value);
					total_haber += parseFloat(haber_colm.item(i).value);
					}
					if(total_debe ==total_haber){
						is_partidaDoble = true;
						$("#validarPartidaDoble").html("<p class ='alert alert-success'>Se cumple partida doble</p>");
						document.getElementById("enviar").removeAttribute("disabled");
						}else{
							is_partidaDoble = false;
							$("#validarPartidaDoble").html("<p class = 'alert alert-danger'>No se cumple partida doble</p>");
							document.getElementById("enviar").setAttribute("disabled","true");
							}
				}
function agregar_IVA(){
	var tipoIVA = cuentas.item(cuentas.length-1).value;
	if(tipoIVA.includes("1105")){
		
		agregarIVA_percibido();
		}else if(tipoIVA.includes("210")){
			
			agregarIVA_retenido();
			}
	}
function agregarIVA_percibido(){
	var total_debe=0;
	var iva_per;
	var cuenta_indice=-1;
	for(var i =0;i< cuentas.length; i++){
		for(var j =0; j<cuentasD.length; j++){
			if(cuentas.item(i).value== cuentasD[j]){
				cuenta_indice = i;
				}
			}
		}
		if(cuenta_indice == -1){
			$('#validarCuentasActivo').html("<p class = 'alert alert-warning'><strong>Inconsistencia en las cuentas:</strong> Por favor revise las cuentas...</p>");
			}
			else{
				$('#validarCuentasActivo').html("");
				iva_per = parseFloat(haber_colm.item(cuenta_indice).value)*0.13;
				iva_per = parseFloat(iva_per.toFixed(2));
				iva_partida_actual = iva_per;
				debe_colm.item(cuentas.length -1).value=iva_per;
				haber_colm.item(cuenta_indice).value = parseFloat(haber_colm.item(cuenta_indice).value) + iva_per;
				}
}
function agregarIVA_retenido(){
	var total_haber=0;
	var iva_ret;
	var cuenta_indice=-1;
	for(var i =0;i< cuentas.length; i++){
		for(var j =0; j<cuentasD.length; j++){
			if(cuentas.item(i).value== cuentasD[j]){
				cuenta_indice = i;
				}
			}
		}
		if(cuenta_indice == -1){
			$('#validarCuentasActivo').html("<p class = 'alert alert-warning'><strong>Inconsistencia en las cuentas:</strong> Por favor revise las cuentas...</p>");
			}
			else{
				$('#validarCuentasActivo').html("");
				iva_ret = parseFloat(debe_colm.item(cuenta_indice).value)*0.13;
				iva_ret = parseFloat(iva_ret.toFixed(2));
				iva_partida_actual = iva_ret;
				haber_colm.item(cuentas.length -1).value=iva_ret;
				debe_colm.item(cuenta_indice).value = parseFloat(debe_colm.item(cuenta_indice).value) + iva_ret;
	}
}
function agregar_calendario(){
	date_today = new Date();
	for( var i=1; i<=31; i++){
		if(i == date_today.getDate()){
			$('#select_dia').append('<option value='+ i +' selected>'+ i +'</option>');
			}else{
		$('#select_dia').append('<option value='+ i +'>'+ i +'</option>');}
		}
	for(var j=0; j<12; j++){
		if(j== date_today.getMonth()){
			$('#select_mes').append('<option value='+ meses[j]+' selected>'+ meses[j] +'</option>');
			}else{
		$('#select_mes').append('<option value='+ meses[j]+'>'+ meses[j] +'</option>');}
		}
	for(var k=2000; k<2035 ;k++){
		if(k == date_today.getFullYear()){
			$('#select_anio').append('<option value='+ k +' selected>'+ k +'</option>');
			}else{
		$('#select_anio').append('<option value='+ k +'>'+ k +'</option>');}
		}
		
	
	}

