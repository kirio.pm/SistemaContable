$( "#tabs" ).tabs();

$( "#agregarEntrada" ).click(function( event ) {
	$( "#formularioEntrada" ).dialog( "open" );

	event.preventDefault();

});



function ingresoTabla(){
	var tabla, cantidad,precioEntrada, monto;
	
	precioEntrada=$("#precioEntrada").val();
	cantidad=$("#cantidadEntrada").val();
	monto=cantidad*precioEntrada;
	tabla="<tr><td>" ;
	tabla+=cantidad;
	tabla+="</td>";

	tabla+="<td>$";
	tabla+=precioEntrada;
	tabla+= "</td>";
	tabla+="<td>$";
	tabla+=monto;
	tabla+="</td></tr>";
	precioEntrada=$("#precioEntrada").val('');
	cantidad=$("#cantidadEntrada").val('');
	return tabla;
	
}



function ingresoTablaEntradaVacia( ){ //si se está desarrollando una salida queda a cero 		
	var tabla, cantidad,precioEntrada, monto;
	
	precioEntrada=0;
	cantidad=0;
	monto=cantidad*precioEntrada;
	tabla="<tr><td>" ;
	tabla+=cantidad;
	tabla+="</td>";

	tabla+="<td>$";
	tabla+=precioEntrada;
	tabla+= "</td>";
	tabla+="<td>$";
	tabla+=monto;
	tabla+="</td></tr>";
	return tabla;
	
}


function ingresoTablaSalida(){ //Solo Cuando se está ingresando una entrada, se deja esta a cero
	var tabla;

	tabla="<tr><td>0</td>";
	tabla+="<td>$0</td>";
	tabla+="<td>$0</td></tr>";
	return tabla;
	
}


function llenarTablaSalida( ){ // Cuando se está ingresando una salida para actualizar datos a la db
	

	var cantidad=$("#cantidadSalida").val();

	 var parametros = {
                "cantidad" : cantidad,
                
        };
	$.ajax({
		url:'funciones2.php',
		data:parametros,
		type:'get',
		dataType:'json',
		success: function(response){
			//alert("hello");
			var precioUnitario, nuevoMonto, tabla;
			precioUnitario=response.precio;
			nuevoMonto=response.monto;
			//cantidad=response.cantidad;
			//$("#resultado").append(response.precio);

			tabla="<tr><td>"
			tabla+=cantidad;
			tabla+="</td>";
			tabla+="<td>$";
			tabla+=precioUnitario;
			tabla+="</td>";
			tabla+="<td>$";
			tabla+=nuevoMonto;
			tabla+="</td></tr>";

			$("#tablaInternaSalida tbody ").append(tabla);

			$("#cantidadSalida").val('');			

	}


	});
		
	

	


	
}



function ingresoTablaSaldo( ){ 
	
	/*if ($("#tablaInternaSaldo tbody tr").length==0) {

	precioEntrada=$("#precioEntrada").val();
	cantidad=$("#cantidadEntrada").val();
	monto=cantidad*precioEntrada;
	tabla="<tr><td>" ;
	tabla+=cantidad;
	tabla+="</td>";

	tabla+="<td>$";
	tabla+=precioEntrada;
	tabla+= "</td>";
	tabla+="<td>$";
	tabla+=monto;
	tabla+="</td></tr>";
	$("#tablaInternaSaldo tbody ").append(tabla);
	}else{*/


			$.ajax({
                
                url:   'funciones1.php',
                type:  'get',
                dataType: 'json',
                success: function(response){
                	
                	//$("#resultado").append(response.precio);
                	var tabla, cantidad,precioEntrada, monto;
                	precioEntrada=response.precio;
                	cantidad=response.cantidad;
                	monto=response.monto;
                	
                	tabla="<tr><td>" ;
					tabla+=cantidad;
					tabla+="</td>";
					tabla+="<td>$";
					tabla+=precioEntrada;
					tabla+= "</td>";
					tabla+="<td>$";
					tabla+=monto;
					tabla+="</td></tr>";
					
					$("#tablaInternaSaldo tbody ").append(tabla);
                }


                              
        });
		

	
					

	


	//}

	
	
}



$( "#formularioEntrada" ).dialog({
	autoOpen: false,
	width: 400, 
	buttons: [
		{
			text: "Ok",
			click: function() {
				ejecuraAjax();
				$("#tablaInternaEntrada tbody ").append(ingresoTabla());
				$("#tablaInternaSalida tbody ").append(ingresoTablaSalida());
				//$("#tablaInternaSaldo tbody ").append(ingresoTablaSaldo(obtenerDatosBD()));
				ingresoTablaSaldo();
				$( this ).dialog( "close" );
			}
		},
		{
			text: "Cancel",
			click: function() {

				$( this ).dialog( "close" );
			}
		}
	]
});


$("#cantidadEntrada").keyup(function(event) {
	/* Act on the event */
	calcularMontoEntrada();	

});
$("#precioEntrada").keyup(function(event) {
	/* Act on the event */
			calcularMontoEntrada();
});


function calcularMontoEntrada(){
		var cantidad,precioEntrada, monto;
				cantidad=$("#cantidadEntrada").val();
				precioEntrada=$("#precioEntrada").val();
				monto=cantidad*precioEntrada;
				$("#montoEntrada").val(monto);

				

};

function ejecuraAjax(){
var cant=$("#cantidadEntrada").val();
var prec=$("#precioEntrada").val();
	 var parametros = {
                "cantidad" : cant,
                "precio" : prec
        };

     $.ajax({
                data:  parametros,
                url:   'funciones.php',
                type:  'get',
                success: function(response){
                	
                }
        });   
      

};








$( "#agregarSalida" ).click(function( event ) {
	$( "#formularioSalida" ).dialog( "open" );

	event.preventDefault();

});


$( "#formularioSalida" ).dialog({
	autoOpen: false,
	width: 400, 
	buttons: [
		{
			text: "Ok",
			click: function() {			
			
				$("#tablaInternaEntrada tbody ").append(ingresoTablaEntradaVacia());
				
				llenarTablaSalida();
				
				
				$( this ).dialog( "close" );
				ingresoTablaSaldo( )
				
			}
		},
		{
			text: "Cancel",
			click: function() {

				$( this ).dialog( "close" );
			}
		}
	]
});



$( "#cerrarPeriodo" ).click(function( event ) {
	$( "#formularioCierrePeriodo" ).dialog( "open" );
	event.preventDefault();

});


$( "#formularioCierrePeriodo" ).dialog({
	autoOpen: false,
	width: 600, 
	buttons: [
		{
			text: "Ok",
			click: function() {
				window.open('EstadoDeCostos.php','Estado de costos');

				generarEstadoDeCostos();	
				$( this ).dialog( "close" );				
				
			

				
			}
		},
		{
			text: "Cancel",
			click: function() {

				$( this ).dialog( "close" );
			}
		}
	]
});


function generarEstadoDeCostos(){

	var ingVenta, costoDeLoVendido, utilidadBruta,  gastosDeOperacion, gastosDeVentas, utilidadesDeOperacion, gastoFinanciero;
	var utilidadesAntesDeOtroIngreso, otrosIngresos, utilidadAntesDeImpuestos, renta, utilidadNeta;

	ingVenta=$("#ingresoPorVenta").val();
	
	gastosDeOperacion=$("#gastoDeOperacion").val();	
	
	gastosDeVentas=$("#gastoDeVenta").val();
	
	gastoFinanciero=$("#gastosFinancieros").val();
	
	otrosIngresos=$("#otrosIngresos").val();
	
	renta=$("#renta").val();

	

 var parametros = {
                "ingresoPorVenta" : ingVenta,
                "GOperacion" : gastosDeOperacion,
                "GVentas": gastosDeVentas,
                "GFinancieros": gastoFinanciero,
                "OIngresos": otrosIngresos,
                "Renta": renta
        };
  		
        

	$.ajax(	{
		data: parametros, 
		type: 'get',
		url: 'finPeriodo.php',
		dataType: 'json',
		success: function(response){
			
			

			
		
		}

		});


}




$("#ingresoPorVenta").keyup(function(event) {

	if ($("#ingresoPorVenta").val()>=100000) {

        $("#renta").val("0.30");
        }else{
        	$("#renta").val("0.25");
        }
	

});