-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 28-11-2016 a las 00:31:08
-- Versión del servidor: 5.5.24-log
-- Versión de PHP: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `contables`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cargos_empleados`
--

CREATE TABLE IF NOT EXISTS `cargos_empleados` (
  `id` int(11) NOT NULL,
  `cargo` varchar(50) CHARACTER SET utf8 COLLATE utf8_spanish2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cargos_empleados`
--

INSERT INTO `cargos_empleados` (`id`, `cargo`) VALUES
(1, 'Gerente General'),
(2, 'Contador'),
(3, 'Encargado de ComercializaciÃ³n'),
(4, 'Encargado de ProducciÃ³n'),
(5, 'Operario'),
(6, 'Motorista'),
(7, 'Auxiliar');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cuenta`
--

CREATE TABLE IF NOT EXISTS `cuenta` (
  `codigo_mayor` int(11) NOT NULL,
  `nombre_cuenta` varchar(60) NOT NULL,
  `descripcion` varchar(75) DEFAULT NULL,
  `tipo_cuenta` int(11) DEFAULT NULL,
  `er` int(11) DEFAULT '0',
  PRIMARY KEY (`codigo_mayor`),
  UNIQUE KEY `codigo_mayor` (`codigo_mayor`),
  KEY `tipo_cuenta` (`tipo_cuenta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `cuenta`
--

INSERT INTO `cuenta` (`codigo_mayor`, `nombre_cuenta`, `descripcion`, `tipo_cuenta`, `er`) VALUES
(1103, 'ESTIMACIÓN PARA CUENTAS INCOBRABLES', 'ESTIMACIÓN PARA CUENTAS INCOBRABLES (CR)', 1, 4),
(1207, 'Estimación para Cuentas Incobrables a L/P', '1207 Estimación para Cuentas Incobrables a L/P', 1, 4),
(2107, 'IVA Percibido', '2107 IVA Percibido', 2, 5),
(2109, 'DIVIDENDOS POR PAGAR', '2109 DIVIDENDOS POR PAGAR', 2, 5),
(2202, 'PRESTAMOS BANCARIOS A LARGO PLAZO', '2202 PRESTAMOS BANCARIOS A LARGO PLAZO', 2, 5),
(4101, 'COMPRAS', '4 CUENTAS DE RESULTADOS DEUDORAS 41 COSTOS Y GASTOS DE OPERACIÓN', 4, 1),
(4102, 'GASTOS SOBRE COMPRAS', '4 CUENTAS DE RESULTADOS DEUDORAS 41 COSTOS Y GASTOS DE OPERACIÓN', 4, 1),
(4103, 'COSTO DE VENTA', '4 CUENTAS DE RESULTADOS DEUDORAS 41 COSTOS Y GASTOS DE OPERACIÓN', 4, 1),
(4104, 'DEVOLUCIONES SOBRE VENTAS', '4 CUENTAS DE RESULTADOS DEUDORAS 41 COSTOS Y GASTOS DE OPERACIÓN', 4, 1),
(4105, 'GASTOS DE VENTA', '4 CUENTAS DE RESULTADOS DEUDORAS 41 COSTOS Y GASTOS DE OPERACIÓN', 4, 1),
(4106, 'GASTOS DE ADMINISTRACION', '4106 GASTOS DE ADMINISTRACION', 4, 1),
(4107, 'OTROS GASTOS OPERATIVO', '4107 OTROS GASTOS OPERATIVOS', 4, 1),
(4108, 'GASTOS FINANCIEROS', '4108 GASTOS FINANCIEROS', 4, 1),
(4201, 'OTROS GASTOS NO OPERACIONALES', '42 COSTOS Y GASTOS DE NO OPERACION', 4, 1),
(4202, 'GASTOS POR IMPUESTOS SOBRE LA RENTA', '42 COSTOS Y GASTOS DE NO OPERACION', 4, 1),
(4301, 'Ingreso Por venta', '4301 ingresos por ventas de la empresa', 4, 0),
(110101, 'Caja chica', 'ACTIVOS CORRIENTES, EFECTIVO Y EQUIVALENTES', 1, 4),
(110102, 'Bancos', 'ACTIVOS CORRIENTES, EFECTIVO Y EQUIVALENTES', 1, 4),
(110103, 'Efectivo', 'ACTIVOS CORRIENTES, EFECTIVO Y EQUIVALENTES', 1, 4),
(110201, 'Cuentas por Cobrar', 'CUENTAS Y DOCUMENTOS POR COBRAR', 1, 4),
(110202, 'Documentos por Cobrar', 'CUENTAS Y DOCUMENTOS POR COBRAR', 1, 4),
(110203, 'Préstamos y Anticipos Al Personal', 'CUENTAS Y DOCUMENTOS POR COBRAR', 1, 4),
(110204, 'Deudores Diversos', 'CUENTAS Y DOCUMENTOS POR COBRAR', 1, 4),
(110205, 'Otras Cuentas Por Cobrar', 'CUENTAS Y DOCUMENTOS POR COBRAR', 1, 4),
(110401, 'Inventarios de productos', 'INVENTARIOS', 1, 4),
(110402, 'Mercadería en tránsito', 'INVENTARIOS', 1, 4),
(110501, 'IVA por compras locales', 'IVA - CREDITO FISCAL', 1, 4),
(110502, 'IVA Crédito Fiscal Remanente', 'IVA - CREDITO FISCAL', 1, 4),
(110503, 'Remanente IVA', 'IVA - CREDITO FISCAL', 1, 4),
(110504, 'IVA Anticipo a Cuenta', 'IVA - CREDITO FISCAL', 1, 4),
(110505, 'IVA - Percibido', 'IVA - CREDITO FISCAL', 1, 4),
(110506, 'IVA - Retenido', 'IVA - CREDITO FISCAL', 1, 4),
(120101, 'Bancos', '12 ACTIVOS NO CORRIENTE - 1201 EFECTIVO RESTRINGIDO', 1, 4),
(120201, 'Terrenos', '1202 PROPIEDAD PLANTA Y EQUIPO', 1, 4),
(120202, 'Edificios', '1202 PROPIEDAD PLANTA Y EQUIPO', 1, 4),
(120203, 'Instalaciones', '1202 PROPIEDAD PLANTA Y EQUIPO', 1, 4),
(120204, 'Mobiliario y Equipo de Oficina', '1202 PROPIEDAD PLANTA Y EQUIPO', 1, 4),
(120205, 'Equipo de transporte', '1202 PROPIEDAD PLANTA Y EQUIPO', 1, 4),
(120206, 'Otros bienes', '1202 PROPIEDAD PLANTA Y EQUIPO', 1, 4),
(120301, 'Depreciación de Edificios', '1203 DEPRECIACION ACUMULADA PROPIEDAD PLANTA Y EQUIPO (CR)', 1, 4),
(120302, 'Depreciación de Instalaciones', '1203 DEPRECIACION ACUMULADA PROPIEDAD PLANTA Y EQUIPO (CR)', 1, 4),
(120303, 'Depreciación de Mobiliario y Equipo', '1203 DEPRECIACION ACUMULADA PROPIEDAD PLANTA Y EQUIPO (CR)', 1, 4),
(120304, 'Depreciación de Equipo de Transporte', '1203 DEPRECIACION ACUMULADA PROPIEDAD PLANTA Y EQUIPO (CR)', 1, 4),
(120305, 'Depreciación de Otros Activos', '1203 DEPRECIACION ACUMULADA PROPIEDAD PLANTA Y EQUIPO (CR)', 1, 4),
(120401, 'Patentes y marcas', '1204 ACTIVOS INTANGIBLES', 1, 4),
(120402, 'Otros intangibles', '1204 ACTIVOS INTANGIBLES', 1, 4),
(120501, 'Patentes y marcas', '1205 AMORTIZACIÓN DE INTANGIBLES', 1, 4),
(120502, 'Otros intangibles', '1205 AMORTIZACIÓN DE INTANGIBLES', 1, 4),
(120601, 'Cuentas por Cobrar Comerciales a L/P', '1206 CUENTAS Y DOCUMENTOS POR COBRAR A LARGO PLAZO', 1, 4),
(120602, 'Otras cuentas por cobrar', '1206 CUENTAS Y DOCUMENTOS POR COBRAR A LARGO PLAZO', 1, 4),
(210101, 'Proveedores', '2 PASIVO, 21 PASIVOS CORRIENTES, 2101 CUENTAS Y DOCUMENTOS POR PAGAR', 2, 5),
(210102, 'Documentos por Pagar', '2 PASIVO, 21 PASIVOS CORRIENTES, 2101 CUENTAS Y DOCUMENTOS POR PAGAR', 2, 5),
(210103, 'Pago a seguridad privada', '2 PASIVO, 21 PASIVOS CORRIENTES, 2101 CUENTAS Y DOCUMENTOS POR PAGAR', 2, 5),
(210201, 'Sobregiros Bancarios', '2102 PRÉSTAMOS Y SOBREGIROS BANCARIOS', 2, 5),
(210202, 'Préstamos a Corto Plazo', '2102 PRÉSTAMOS Y SOBREGIROS BANCARIOS', 2, 5),
(210203, 'Porción Circulante de Préstamos a Largo Plazo', '2102 PRÉSTAMOS Y SOBREGIROS BANCARIOS', 2, 5),
(210204, 'Otros Préstamos', '2102 PRÉSTAMOS Y SOBREGIROS BANCARIOS', 2, 5),
(210301, 'Salarios', '2103 REMUNERACIONES Y PRESTACIONES POR PAGAR A EMPLEADOS', 2, 5),
(210302, 'Comisiones', '2103 REMUNERACIONES Y PRESTACIONES POR PAGAR A EMPLEADOS', 2, 5),
(210303, 'Vacaciones', '2103 REMUNERACIONES Y PRESTACIONES POR PAGAR A EMPLEADOS', 2, 5),
(210304, 'Aguinaldos', '2103 REMUNERACIONES Y PRESTACIONES POR PAGAR A EMPLEADOS', 2, 5),
(210401, 'Cotizaciones al Seguro Social Salud', '2104 RETENCIONES Y DESCUENTOS', 2, 5),
(210402, 'Cotizaciones a Fondos de Pensiones', '2104 RETENCIONES Y DESCUENTOS', 2, 5),
(210403, 'Retenciones de Impuesto sobre la renta', '2104 RETENCIONES Y DESCUENTOS', 2, 5),
(210501, 'IVA Débito Fiscal Contribuyente', '2105 IVA DEBITO FISCAL', 2, 5),
(210502, 'IVA Débito Fiscal Consumidor Final 2106 IVA Retenido a Terce', '2105 IVA DEBITO FISCAL', 2, 5),
(210801, 'Impuesto Sobre la Renta Corriente', '2108 IMPUESTOS POR PAGAR', 2, 5),
(210802, 'IVA por Pagar', '2108 IMPUESTOS POR PAGAR', 2, 5),
(210803, 'Provisión por Pago a Cuenta', '2108 IMPUESTOS POR PAGAR', 2, 5),
(220101, 'Cuentas por Pagar', '22 PASIVOS NO CORRIENTES 2201 CUENTAS Y DOCUMENTOS POR PAGAR', 2, 5),
(220102, 'Documentos por pagar', '22 PASIVOS NO CORRIENTES 2201 CUENTAS Y DOCUMENTOS POR PAGAR', 2, 5),
(330101, 'De ejercicios Anteriores', '32 RESULTADOS 3201 UTILIDADES NO DISTRIBUIDAS', 3, 3),
(330102, 'Del Presente Ejercicio', '32 RESULTADOS 3201 UTILIDADES NO DISTRIBUIDAS', 3, 3),
(410501, 'Salarios', '4105 GASTOS DE VENTA', 4, 1),
(410502, 'Horas extras', '4105 GASTOS DE VENTA', 4, 1),
(410503, 'Vacaciones', '4105 GASTOS DE VENTA', 4, 1),
(410504, 'Aguinaldos', '4105 GASTOS DE VENTA', 4, 1),
(410505, 'Seguro Social', '4105 GASTOS DE VENTA', 4, 1),
(410506, 'AFP', '4105 GASTOS DE VENTA', 4, 1),
(410507, 'Viáticos', '4105 GASTOS DE VENTA', 4, 1),
(410508, 'Comunicaciones', '4105 GASTOS DE VENTA', 4, 1),
(410509, 'Alquileres', '4105 GASTOS DE VENTA', 4, 1),
(410510, 'Luz y agua', '4105 GASTOS DE VENTA', 4, 1),
(410511, 'Combustibles y lubricantes', '4105 GASTOS DE VENTA', 4, 1),
(410512, 'Mantenimiento y repuestos de vehículos', '4105 GASTOS DE VENTA', 4, 1),
(410513, 'Cuentas incobrables', '4105 GASTOS DE VENTA', 4, 1),
(410514, 'Obsolescencia de inventarios', '4105 GASTOS DE VENTA', 4, 1),
(410515, 'Publicidad y promoción', '4105 GASTOS DE VENTA', 4, 1),
(410516, 'Papelería y útiles', '4105 GASTOS DE VENTA', 4, 1),
(410517, 'Materiales de limpieza', '4105 GASTOS DE VENTA', 4, 1),
(410518, 'Mantenimiento', '4105 GASTOS DE VENTA', 4, 1),
(410519, 'Depreciaciones', '4105 GASTOS DE VENTA', 4, 1),
(410520, 'Amortización', '4105 GASTOS DE VENTA', 4, 1),
(410601, 'Sueldos', '4106 GASTOS DE ADMINISTRACION', 4, 1),
(410602, 'Servicios profesionales', '4106 GASTOS DE ADMINISTRACION', 4, 1),
(410603, 'Vacaciones', '4106 GASTOS DE ADMINISTRACION', 4, 1),
(410604, 'Aguinaldos', '4106 GASTOS DE ADMINISTRACION', 4, 1),
(410606, 'Indemnizaciones', '4106 GASTOS DE ADMINISTRACION', 4, 1),
(410607, 'Seguro Social', '4106 GASTOS DE ADMINISTRACION', 4, 1),
(410608, 'AFP', '4106 GASTOS DE ADMINISTRACION', 4, 1),
(410609, 'Viáticos', '4106 GASTOS DE ADMINISTRACION', 4, 1),
(410610, 'Asesoría técnica', '4106 GASTOS DE ADMINISTRACION', 4, 1),
(410611, 'Equipo y accesorios p/oficina', '4106 GASTOS DE ADMINISTRACION', 4, 1),
(410612, 'Papelería y útiles', '4106 GASTOS DE ADMINISTRACION', 4, 1),
(410613, 'Teléfono/fax', '4106 GASTOS DE ADMINISTRACION', 4, 1),
(410614, 'Servicios de internet', '4106 GASTOS DE ADMINISTRACION', 4, 1),
(410615, 'Electricidad', '4106 GASTOS DE ADMINISTRACION', 4, 1),
(410616, 'Materiales de Limpieza', '4106 GASTOS DE ADMINISTRACION', 4, 1),
(410617, 'Mantenimiento', '4106 GASTOS DE ADMINISTRACION', 4, 1),
(410618, 'Depreciaciones', '4106 GASTOS DE ADMINISTRACION', 4, 1),
(410619, 'Amortización', '4106 GASTOS DE ADMINISTRACION', 4, 1),
(410801, 'Intereses', '4108 GASTOS FINANCIEROS', 4, 1),
(420201, 'GASTOS POR IMPUESTOS SOBRE LA RENTA CORRIENTE', '4202 GASTOS POR IMPUESTOS SOBRE LA RENTA', 4, 1),
(420202, 'GASTOS POR IMP SOBRE LA RENTA DIFERIDO - ACTIVO', '4202 GASTOS POR IMPUESTOS SOBRE LA RENTA', 4, 1),
(420203, 'GASTOS POR IMP SOBRE LA RENTA DIFERIDO - PASIVO', '4202 GASTOS POR IMPUESTOS SOBRE LA RENTA', 4, 1),
(11050701, 'Suministros de Oficina', '110507 PAGOS ANTICIPADOS', 1, 4),
(11050702, 'Mantenimientos', '110507 PAGOS ANTICIPADOS', 1, 4),
(11050704, 'Publicidad y Promoción', '110507 PAGOS ANTICIPADOS', 1, 4),
(11050705, 'Beneficios o prestaciones a empleados', '110507 PAGOS ANTICIPADOS', 1, 4),
(11050706, 'Gastos de organización', '110507 PAGOS ANTICIPADOS', 1, 4),
(11050707, 'Pago a Cuenta Impuesto sobre la Renta', '110507 PAGOS ANTICIPADOS', 1, 4),
(11050708, 'Otros Pagos Anticipados', '110507 PAGOS ANTICIPADOS', 1, 4),
(21010201, 'Contratos a Corto Plazo', '210102 Documentos por Pagar', 2, 5),
(21010202, 'Pagarés', '210102 Documentos por Pagar', 2, 5),
(21040201, 'ISSS', '', 2, 5),
(21040202, 'AFP Confía', '', 2, 5),
(22010201, 'Letras de cambio', ' ', 2, 5),
(22010202, 'Pagarés', ' ', 2, 5),
(31010101, 'Capital Social Pagado ', '3 PATRIMONIO DE LOS ACCIONISTAS	31 CAPITAL SOCIAL 3101 CAPITAL SOCIAL', 3, 3),
(31010102, 'Capital Social no Pagado', '3 PATRIMONIO DE LOS ACCIONISTAS	31 CAPITAL SOCIAL 3101 CAPITAL SOCIAL', 3, 3);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle_libro_diario`
--

CREATE TABLE IF NOT EXISTS `detalle_libro_diario` (
  `id_movimiento` int(11) NOT NULL,
  `cuenta` int(11) NOT NULL,
  `deber` decimal(12,2) DEFAULT NULL,
  `haber` decimal(12,2) DEFAULT NULL,
  KEY `id_movimiento` (`id_movimiento`),
  KEY `cuenta` (`cuenta`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `detalle_libro_diario`
--

INSERT INTO `detalle_libro_diario` (`id_movimiento`, `cuenta`, `deber`, `haber`) VALUES
(26, 110103, '25000.00', '0.00'),
(26, 31010101, '0.00', '25000.00'),
(27, 210102, '0.00', '2000.00'),
(27, 11050701, '800.00', '0.00'),
(27, 120204, '1200.00', '0.00'),
(28, 110103, '1000.00', '0.00'),
(28, 4301, '0.00', '1000.00'),
(29, 110103, '0.00', '15000.00'),
(29, 11050701, '15000.00', '0.00'),
(30, 110202, '900.00', '0.00'),
(30, 4301, '0.00', '900.00'),
(31, 110103, '0.00', '1200.00'),
(31, 210102, '1200.00', '0.00'),
(32, 210102, '0.00', '230.00'),
(32, 410510, '230.00', '0.00'),
(33, 110103, '0.00', '600.00'),
(33, 410501, '600.00', '0.00'),
(34, 110103, '0.00', '120.00'),
(34, 210102, '120.00', '0.00'),
(35, 110103, '700.00', '0.00'),
(35, 4301, '0.00', '700.00'),
(36, 110202, '800.00', '0.00'),
(36, 4301, '0.00', '800.00'),
(37, 110103, '0.00', '110.00'),
(37, 210102, '110.00', '0.00'),
(38, 110103, '1500.00', '0.00'),
(38, 4301, '0.00', '1500.00'),
(39, 110103, '0.00', '600.00'),
(39, 410509, '600.00', '0.00'),
(40, 110103, '0.00', '500.00'),
(40, 410501, '500.00', '0.00'),
(41, 110103, '0.00', '2000.00'),
(41, 410612, '2000.00', '0.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `empleados`
--

CREATE TABLE IF NOT EXISTS `empleados` (
  `id` int(11) NOT NULL,
  `codigo_empleado` varchar(8) NOT NULL,
  `primer_nombre` varchar(50) DEFAULT NULL,
  `segundo_nombre` varchar(50) DEFAULT NULL,
  `primer_apellido` varchar(50) DEFAULT NULL,
  `segundo_apellido` varchar(50) DEFAULT NULL,
  `cargo` varchar(30) DEFAULT NULL,
  `salario_mensual_contratado` double NOT NULL,
  `isss_trabajador` double DEFAULT NULL,
  `isss_patrono` double DEFAULT NULL,
  `afp_trabajador` double DEFAULT NULL,
  `afp_patrono` double DEFAULT NULL,
  `salario_diario` double DEFAULT NULL,
  `vacaciones` double DEFAULT NULL,
  `aguinaldo` double DEFAULT NULL,
  `salario_mensual` double DEFAULT NULL,
  `aportaciones_mensuales_patrono` double DEFAULT NULL,
  `pago_salario_patrono` double DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `empleados`
--

INSERT INTO `empleados` (`id`, `codigo_empleado`, `primer_nombre`, `segundo_nombre`, `primer_apellido`, `segundo_apellido`, `cargo`, `salario_mensual_contratado`, `isss_trabajador`, `isss_patrono`, `afp_trabajador`, `afp_patrono`, `salario_diario`, `vacaciones`, `aguinaldo`, `salario_mensual`, `aportaciones_mensuales_patrono`, `pago_salario_patrono`) VALUES
(0, 'AA111122', 'Kirio', 'Waldo', 'Portillo', 'Mendoza', 'Motorista', 125, 3.75, 9.375, 7.8125, 8.4375, 4.1666666666667, 16.25, 3.4722222222222, 156.28472222222, 8.4375, 164.72222222222);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `libro_diario`
--

CREATE TABLE IF NOT EXISTS `libro_diario` (
  `id_movimiento` int(11) NOT NULL AUTO_INCREMENT,
  `dia` int(11) NOT NULL,
  `mes` varchar(11) NOT NULL,
  `ano` int(11) NOT NULL,
  `partida` int(11) NOT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_movimiento`),
  UNIQUE KEY `id_movimiento` (`id_movimiento`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=42 ;

--
-- Volcado de datos para la tabla `libro_diario`
--

INSERT INTO `libro_diario` (`id_movimiento`, `dia`, `mes`, `ano`, `partida`, `descripcion`) VALUES
(26, 27, 'Noviembre', 2016, 1, ''),
(27, 27, 'Noviembre', 2016, 2, ''),
(28, 27, 'Noviembre', 2016, 3, ''),
(29, 27, 'Noviembre', 2016, 4, ''),
(30, 27, 'Noviembre', 2016, 5, ''),
(31, 27, 'Noviembre', 2016, 6, ''),
(32, 27, 'Noviembre', 2016, 7, ''),
(33, 27, 'Noviembre', 2016, 8, ''),
(34, 27, 'Noviembre', 2016, 9, ''),
(35, 27, 'Noviembre', 2016, 10, ''),
(36, 27, 'Noviembre', 2016, 11, ''),
(37, 27, 'Noviembre', 2016, 12, ''),
(38, 27, 'Noviembre', 2016, 13, ''),
(39, 27, 'Noviembre', 2016, 14, ''),
(40, 27, 'Noviembre', 2016, 15, ''),
(41, 27, 'Noviembre', 2016, 16, '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipo_cuenta`
--

CREATE TABLE IF NOT EXISTS `tipo_cuenta` (
  `id_tipo_cuenta` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_tipo_cuenta` varchar(20) NOT NULL,
  PRIMARY KEY (`id_tipo_cuenta`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Volcado de datos para la tabla `tipo_cuenta`
--

INSERT INTO `tipo_cuenta` (`id_tipo_cuenta`, `nombre_tipo_cuenta`) VALUES
(1, 'Activo'),
(2, 'Pasivo'),
(3, 'Capital'),
(4, 'Resultado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE IF NOT EXISTS `usuario` (
  `usuario` char(15) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `password` varchar(40) NOT NULL,
  `fecha` date NOT NULL,
  `tipo` varchar(25) NOT NULL,
  PRIMARY KEY (`usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`usuario`, `password`, `fecha`, `tipo`) VALUES
('kirio', 'd033e22ae348aeb5660fc2140aec35850c4da997', '2013-12-01', 'administrador');

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cuenta`
--
ALTER TABLE `cuenta`
  ADD CONSTRAINT `cuenta_ibfk_1` FOREIGN KEY (`tipo_cuenta`) REFERENCES `tipo_cuenta` (`id_tipo_cuenta`) ON DELETE SET NULL;

--
-- Filtros para la tabla `detalle_libro_diario`
--
ALTER TABLE `detalle_libro_diario`
  ADD CONSTRAINT `detalle_libro_diario_ibfk_1` FOREIGN KEY (`id_movimiento`) REFERENCES `libro_diario` (`id_movimiento`) ON DELETE CASCADE,
  ADD CONSTRAINT `detalle_libro_diario_ibfk_2` FOREIGN KEY (`cuenta`) REFERENCES `cuenta` (`codigo_mayor`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
