<?php 
    error_reporting(E_ALL ^ E_NOTICE);

    if($_COOKIE["sesion"])
    {
        header("Location: php/inicio.php");
    }
?>
<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <meta name="description" content="Este es un sistema de contabilidad basado en la web, para administrar los procesos contables de la Propuesta para el desarrollo de la actividad de venta de Pan Frances y Pan Dulce."/>

    <!-- Estilos del sitio -->
    <link rel="stylesheet" href="css/bootstrap.min.css"/>
    <link rel="stylesheet" href="css/estilos.css"/>
    <link rel="stylesheet" href="css/login.css"/>
    
    <script>
        !window.jQuery && document.write("<script src='js/jquery.min.js'><\/script>");
    </script>

    <script>
        var nav = navigator.appName;
        if(nav=="Microsoft Internet Explorer"){
            alert("Está usando "+nav+". Puede que el sistema no funcione correctamente.");
        };
    </script>

    <title>Panadería | SIC115</title>
</head>

<body>
<div class="container">
        <br>
        <?php
            error_reporting(E_ALL ^ E_NOTICE);
            
            if ($_GET["error"]=="si")
            {
                echo "<div class='alert alert-danger alert-dismissable'>";
                echo "<button type='button' class='close' data-dismiss='alert'>&times;</button>";
                echo "Nombre de usuario o contraseña inválidos. Por favor, verifique sus datos.";
                echo "</div>";
            }
        ?>

        <div class="text-center">
             <font face="Open Sans" size="14" color="white">¡Bienvenido a Panadería FRESHBREAD!</font>
             <font face="Open Sans" size="3" color="white"><p>Para utilizar todas las funciones del sistema contable, usted deberá iniciar sesión previamente.</p></font> 
        </div>

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="pr-wrap">
                
            </div>
            <div class="wrap">
                <p class="form-title">Iniciar Sesión</p>
                <form class="login"
                maxlength= "15" 
                id="login" 
                name="login_frm" 
                method="post" 
                action="php/control.php" 
                enctype="application/x-www-form-urlencoded" 
                role="form" 
                onSubmit="return validacion()"
                >
                <input type="text"
                maxlength= "15"
                    class="form-control" 
                    id="user" 
                    name="user_txt" 
                    placeholder="Usuario" required/>
                
                <input type="password" 
                maxlength= "20"
                        class="form-control" 
                        id="password" 
                        name="password_txt" 
                        placeholder="Contraseña" required/>

                <input type="submit"   
                        value="Entrar" 
                        class="btn btn-success btn-sm" />
                
                </form>
            </div>
        </div>
    </div>
    <div class="posted-by"><a href="http://www.jquery2dotnet.com">Panadería | SIC115</a></div>
</div>
    
    <script src="js/validaciones.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/jquery.min.js"></script>

</body>
</html>